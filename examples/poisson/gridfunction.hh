#ifndef EXAMPLES_POISSON_GRIDFUNCTION_HH
#define EXAMPLES_POISSON_GRIDFUNCTION_HH

#include <string>

#include <dune/fem/function/grid/localizablefunction.hh>

// gridFunction
// ------------

template< class Function, class GridPart >
Dune::Fem::LocalizableFunction< Function, GridPart >
gridFunction ( const std::string &name, const Function &function, const GridPart &gridPart, int order )
{
  return Dune::Fem::LocalizableFunction< Function, GridPart >( name, function, gridPart, order );
}

#endif // #ifndef EXAMPLES_POISSON_GRIDFUNCTION_HH
