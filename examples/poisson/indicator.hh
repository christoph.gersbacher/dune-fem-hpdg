#ifndef EXAMPLES_POISSON_INDICATOR_HH
#define EXAMPLES_POISSON_INDICATOR_HH

#include <cassert>
#include <cmath>
#include <cstddef>

#include <algorithm>
#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/common/dynvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>

#include <dune/geometry/type.hh>

#include <dune/fem/common/coordinate.hh>
#include <dune/fem/function/localfunction/continuous.hh>
#include <dune/fem/function/localfunction/localfunction.hh>
#include <dune/fem/hpdg/space/shapefunctionset/orthonormal.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/operator/projection/local/l2projection.hh>
#include <dune/fem/operator/projection/local/riesz.hh>
#include <dune/fem/quadrature/geometric/gausslegendre.hh>
#include <dune/fem/quadrature/quadrature.hh>
#include <dune/fem/space/basisfunctionset/default.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/shapefunctionset/vectorial.hh>
#include <dune/fem/utility/locall2norm.hh>

#include "diameter.hh"

namespace __PoissonErrorIndicator
{

  // BasisFunctionSets
  // -----------------

  template< class Entity, class Range >
  class BasisFunctionSets
  {
    /** \brief entity type */
    using EntityType = Entity;
    /** \brief key type */
    using KeyType = int;

  private:
    using ScalarShapeFunctionSetType = Dune::Fem::hpDG::OrthonormalShapeFunctionSet< Dune::Fem::FunctionSpace< typename EntityType::Geometry::ctype, typename Range::value_type, EntityType::mydimension, 1 > >;
    using ShapeFunctionSetType = Dune::Fem::VectorialShapeFunctionSet< ScalarShapeFunctionSetType, Range >;

  public:
    /** \brief basis function set type */
    using BasisFunctionSetType = Dune::Fem::DefaultBasisFunctionSet< EntityType, ShapeFunctionSetType >;

    /** \brief get basis function set of given order for entity */
    static BasisFunctionSetType basisFunctionSet ( const EntityType &entity, int order )
    {
      return BasisFunctionSetType( entity, shapeFunctionSet( entity.type(), order ) );
    }

  private:
    static ShapeFunctionSetType shapeFunctionSet ( Dune::GeometryType type, int order )
    {
      return ShapeFunctionSetType( ScalarShapeFunctionSetType( type, order ) );
    }
  };



  // LocalL2ProjectionBase
  // ---------------------

  template< class BasisFunctionSet >
  class LocalL2ProjectionBase
    : public Dune::Fem::LocalL2Projection< BasisFunctionSet, LocalL2ProjectionBase< BasisFunctionSet > >
  {
    using ThisType = LocalL2ProjectionBase< BasisFunctionSet >;
    using BaseType = Dune::Fem::LocalL2Projection< BasisFunctionSet, LocalL2ProjectionBase< BasisFunctionSet > >;

  public:
    /** \brief basis function set type */
    using BasisFunctionSetType = typename BaseType::BasisFunctionSetType;

  private:
    using EntityType = typename BasisFunctionSetType::EntityType;
    using QuadratureType = Dune::Fem::GaussLegendreQuadrature< typename EntityType::Geometry::ctype, EntityType::mydimension >;
    using LocalRieszProjectionType = Dune::Fem::DenseLocalRieszProjection< BasisFunctionSetType, QuadratureType >;
    using ImplementationType = Dune::Fem::DefaultLocalL2Projection< LocalRieszProjectionType, QuadratureType >;

  public:
    /** \name Construction
     *  \{
     */

    explicit LocalL2ProjectionBase ( const BasisFunctionSetType &basisFunctionSet )
      : impl_( basisFunctionSet )
    {}

    explicit LocalL2ProjectionBase ( BasisFunctionSetType &&basisFunctionSet )
      : impl_( std::forward< BasisFunctionSetType >( basisFunctionSet ) )
    {}

    LocalL2ProjectionBase ( const ThisType & ) = default;

    LocalL2ProjectionBase ( ThisType && ) = default;

    /** \} */

    /** \name Assignment
     *  \{
     */

    ThisType &operator= ( const ThisType & ) = default;

    ThisType &operator= ( ThisType && ) = default;

    /** \} */


    /** \name Public member methods
     *  \{
     */

    BasisFunctionSetType basisFunctionSet () const
    {
      return impl_.basisFunctionSet();
    }

    template< class LocalFunction, class LocalDofVector >
    void operator() ( const LocalFunction &localFunction, LocalDofVector &localDofVector ) const
    {
      impl_( localFunction, localDofVector );
    }

    template< class LocalFunction, class LocalDofVector >
    void apply ( const LocalFunction &localFunction, LocalDofVector &localDofVector ) const
    {
      impl_.apply( localFunction, localDofVector );
    }

    /** \} */

  private:
    ImplementationType impl_;
  };



  // LocalL2Projection
  // -----------------

  template< class Entity, class Range >
  class LocalL2Projection
    : public LocalL2ProjectionBase< typename BasisFunctionSets< Entity, Range >::BasisFunctionSetType >
  {
    using BaseType = LocalL2ProjectionBase< typename BasisFunctionSets< Entity, Range >::BasisFunctionSetType >;

  public:
    using BasisFunctionSetType = typename BaseType::BasisFunctionSetType;
    using BaseType::basisFunctionSet;

    LocalL2Projection ( const Entity &entity, int order )
      : BaseType( basisFunctionSet( entity, order ) )
    {}

  private:
    static BasisFunctionSetType basisFunctionSet ( const Entity &entity, int order )
    {
      return BasisFunctionSets< Entity, Range >::basisFunctionSet( entity, order );
    }
  };



  // Trace
  // -----

  template< class Intersection, class Range, class Implementation >
  class Trace
    : public Dune::Fem::DefaultContinuousLocalFunction< Intersection, Range, Implementation >
  {
    using BaseType = Dune::Fem::DefaultContinuousLocalFunction< Intersection, Range, Implementation >;

  public:
    /** \name Construction
     *  \{
     */

    explicit Trace ( const typename BaseType::EntityType &intersection )
      : intersection_( intersection )
    {}

    /** \} */

    /** \name Public member methods
     *  \{
     */

    int order () const
    {
      DUNE_THROW( Dune::NotImplemented, "Method order() not implemented yet" );
    }

    template< class Point >
    void evaluate ( const Point &x, typename BaseType::RangeType &value ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Method evaluate() not implemented yet" );
    }

    template< class Point >
    void jacobian ( const Point &x, typename BaseType::JacobianRangeType &jacobian ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Method jacobian() not implemented yet" );
    }

    template< class Point >
    void hessian ( const Point &x, typename BaseType::HessianRangeType &hessian ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Method hessian() not implemented yet" );
    }

    const typename BaseType::EntityType &entity () const { return intersection_.get(); }

    /** \} */

  protected:
    std::reference_wrapper< const Intersection > intersection_;
  };



  // BoundaryTrace
  // -------------

  template< class G, class Uh >
  class BoundaryTrace
    : public Trace< typename G::EntityType, typename G::RangeType, BoundaryTrace< G, Uh > >
  {
    using BaseType = Trace< typename G::EntityType, typename G::RangeType, BoundaryTrace< G, Uh > >;

  public:
    using RangeFieldType = typename BaseType::RangeFieldType;
    using RangeType = typename BaseType::RangeType;

  private:
    using IntersectionType = typename BaseType::EntityType;
    using LocalGeometryType = typename IntersectionType::LocalGeometry;

  private:
    using LocalDofVectorType = Dune::DynamicVector< RangeFieldType >;
    using LocalL2ProjectionType = __PoissonErrorIndicator::LocalL2Projection< IntersectionType, RangeType >;
    using ProjectedLocalFunctionType = Dune::Fem::LocalFunction< typename LocalL2ProjectionType::BasisFunctionSetType, LocalDofVectorType >;

  public:
    using BaseType::entity;

    /** \name Construction
     *  \{
     */

    explicit BoundaryTrace ( const G &g, const Uh &uh )
      : BaseType( g.entity() ),
        uh_( uh ),
        localGeometry_( entity().geometryInInside() )
    {
      const LocalL2ProjectionType l2Projection( entity(), uh.order() );
      gh_.init( l2Projection.basisFunctionSet() );
      l2Projection( g, gh_ );
    }

    /** \} */

    /** \name Public member methods
     *  \{
     */

    int order () const { return uh().order(); }

    template< class Point >
    void evaluate ( const Point &x, typename BaseType::RangeType &value ) const
    {
      const typename LocalGeometryType::LocalCoordinate y = Dune::Fem::coordinate( x );
      const auto z = localGeometry().global( y );

      RangeType w;
      uh().evaluate( z, w );
      value = w;

      gh().evaluate( y, w );
      value -= w;
    }

    /** \} */

  protected:
    const ProjectedLocalFunctionType &gh() const { return gh_; }

    const Uh &uh() const { return uh_; }

    const LocalGeometryType &localGeometry () const { return localGeometry_; }

    ProjectedLocalFunctionType gh_;
    Uh uh_;
    LocalGeometryType localGeometry_;
  };



  // InteriorTrace
  // -------------

  template< class Intersection, class LocalFunction, class Implementation >
  class InteriorTrace
    : public Trace< Intersection, typename LocalFunction::RangeType, Implementation >
  {
    using BaseType = Trace< Intersection, typename LocalFunction::RangeType, Implementation >;

  protected:
    using Inside = std::integral_constant< std::size_t, 0u >;
    using Outside = std::integral_constant< std::size_t, 1u >;

  public:
    /** \brief local function type */
    using LocalFunctionType = LocalFunction;

  private:
    using LocalGeometryType = typename Intersection::LocalGeometry;

  public:
    /** \name Construction
     *  \{
     */

    explicit InteriorTrace ( const Intersection &intersection, const LocalFunctionType &inside, const LocalFunctionType &outside )
      : BaseType( intersection ),
        localFunctions_( inside, outside ),
        localGeometries_( intersection.geometryInInside(), intersection.geometryInOutside() )
    {}

    /** \} */

    /** \name Public member methods
     *  \{
     */

    int order () const
    {
      return std::max( localFunction( Inside() ).order(), localFunction( Outside() ).order() );
    }

    /** \} */

  protected:
    template< class Tag >
    const LocalFunctionType &localFunction ( Tag ) const
    {
      return std::get< Tag::value >( localFunctions_ );
    }

    template< class Tag >
    const LocalGeometryType &localGeometry ( Tag ) const
    {
      return std::get< Tag::value >( localGeometries_ );
    }

  private:
    std::tuple< std::reference_wrapper< const LocalFunctionType >, std::reference_wrapper< const LocalFunctionType > > localFunctions_;
    std::tuple< LocalGeometryType, LocalGeometryType > localGeometries_;
  };



  // Jump
  // ----

  template< class Intersection, class LocalFunction >
  class Jump
    : public InteriorTrace< Intersection, LocalFunction, Jump< Intersection, LocalFunction > >
  {
    using BaseType = InteriorTrace< Intersection, LocalFunction, Jump< Intersection, LocalFunction > >;

  public:
    using LocalFunctionType = typename BaseType::LocalFunctionType;

    using RangeType = typename BaseType::RangeType;

    using LocalCoordinateType = typename BaseType::LocalCoordinateType;

  public:
    /** \name Construction
     *  \{
     */

    explicit Jump ( const Intersection &intersection, const LocalFunction &inside, const LocalFunction &outside )
      : BaseType( intersection, inside, outside  )
    {}

    /** \} */

    /** \name Public member methods
     *  \{
     */

    template< class Point >
    void evaluate ( const Point &x, RangeType &value ) const
    {
      const LocalCoordinateType y = Dune::Fem::coordinate( x );
      value = evaluate< typename BaseType::Inside >( y );
      value -= evaluate< typename BaseType::Outside >( y );
    }

    /** \} */

  protected:
    using BaseType::localFunction;
    using BaseType::localGeometry;

  private:
    template< class Tag >
    RangeType evaluate ( const LocalCoordinateType &y ) const
    {
      RangeType value;
      const auto z = localGeometry( Tag() ).global( y );
      localFunction( Tag() ).evaluate( z, value );
      return std::move( value );
    }
  };



  // NormalDerivativeJump
  // --------------------

  template< class Intersection, class LocalFunction >
  class NormalDerivativeJump
    : public InteriorTrace< Intersection, LocalFunction, NormalDerivativeJump< Intersection, LocalFunction > >
  {
    using BaseType = InteriorTrace< Intersection, LocalFunction, NormalDerivativeJump< Intersection, LocalFunction > >;

  public:
    using LocalFunctionType = typename BaseType::LocalFunctionType;

    using DomainType = typename BaseType::DomainType;
    using RangeType = typename BaseType::RangeType;
    using JacobianRangeType = typename BaseType::JacobianRangeType;

    using LocalCoordinateType = typename BaseType::LocalCoordinateType;

  public:
    /** \name Construction
     *  \{
     */

    explicit NormalDerivativeJump ( const Intersection &intersection, const LocalFunction &inside, const LocalFunction &outside )
      : BaseType( intersection, inside, outside  )
    {}

    /** \} */

    /** \name Public member methods
     *  \{
     */

    template< class Point >
    void evaluate ( const Point &x, typename BaseType::RangeType &value ) const
    {
      const LocalCoordinateType y = Dune::Fem::coordinate( x );
      const DomainType normal = unitOuterNormal( y );

      JacobianRangeType jacobian;
      this->template jacobian< typename BaseType::Inside >( y, jacobian );
      jacobian.mv( normal, value );

      this->template jacobian< typename BaseType::Outside >( y, jacobian );
      jacobian.mmv( normal, value );
    }

    /** \} */

  protected:
    using BaseType::localGeometry;
    using BaseType::localFunction;

  private:
    DomainType unitOuterNormal ( const LocalCoordinateType &y ) const
    {
      return BaseType::entity().unitOuterNormal( y );
    }

    template< class Tag >
    void jacobian ( const LocalCoordinateType &y, JacobianRangeType &jacobian ) const
    {
      const auto z = localGeometry( Tag() ).global( y );
      localFunction( Tag() ).jacobian( z, jacobian );
    }

    template< class Tag >
    RangeType evaluate ( const LocalCoordinateType &y, const DomainType &normal )
    {
      JacobianRangeType jacobian;
      const auto z = localGeometry( Tag() ).global( y );
      localFunction( Tag() ).jacobian( z, jacobian );

      RangeType value;
      jacobian.mv( normal, value );

      return std::move( value );
    }
  };



  // Residual
  // --------

  template< class F, class Uh >
  class Residual
    : public Dune::Fem::DefaultContinuousLocalFunction< typename F::EntityType, typename F::RangeType, Residual< F, Uh > >
  {
    using BaseType = Dune::Fem::DefaultContinuousLocalFunction< typename F::EntityType, typename F::RangeType, Residual< F, Uh > >;

  public:
    static const int dimDomain = BaseType::dimDomain;
    static const int dimRange = BaseType::dimRange;

    using RangeFieldType = typename BaseType::RangeFieldType;

    explicit Residual ( const F &f, const Uh &uh )
      : f_( f ),
        uh_( uh )
    {}

    int order () const { return std::max( f().order(), uh().order() ); }

    template< class Point >
    void evaluate ( const Point &x, typename BaseType::RangeType &value ) const
    {
      f().evaluate( x, value );
      uh().hessian( x, hessian_ );
      for( int i = 0; i < dimRange; ++i )
        value[ i ] += trace( hessian_[ i ] );
    }

    template< class Point >
    void jacobian ( const Point &x, typename BaseType::JacobianRangeType &jacobian ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Method jacobian() not implemented yet" );
    }

    template< class Point >
    void hessian ( const Point &x, typename BaseType::HessianRangeType &hessian ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Method hessian() not implemented yet" );
    }

    const typename BaseType::EntityType &entity () const { return f().entity(); }

  private:
    static RangeFieldType trace ( const Dune::FieldMatrix< RangeFieldType, dimDomain, dimDomain > &matrix )
    {
      RangeFieldType trace = 0;
      for( int i = 0; i < dimDomain; ++i )
        trace += matrix[ i ][ i ];
      return trace;
    }

    const F &f() const { return f_; }

    const Uh &uh() const { return uh_; }

    F f_;
    Uh uh_;

    mutable typename Uh::HessianRangeType hessian_;
  };

} // namespace __PoissonErrorIndicator



// PoissonErrorEstimate
// --------------------

/** \brief please doc me
 *
 *  This is an implemenation of the error indicator presented in:
 *  <blockquote>
 *    P. Houston, E. Süli, and T. Wihler. A posteriori error analysis of
 *    \f$hp\f$-version discontinuous Galerkin finite-element methods for
 *    second-order quasi-linear elliptic PDEs. IMA Journal of Numerical
 *    Analysis, 28 (2008), pp. 245--273.
 *  </blockquote>

 *  \tparam  GridFunction  right hand side \f$f\$f in Poisson's equation
 *
 *  \ingroup Indicator
 */
template< class GridFunction, class BoundaryData, class Penalty >
class PoissonErrorEstimate final
{
  using ThisType = PoissonErrorEstimate< GridFunction, BoundaryData, Penalty >;

public:
  /** \brief grid function type */
  using GridFunctionType = GridFunction;
  /** \brief boundary data type */
  using BoundaryDataType = BoundaryData;
  /** \brief penalty type */
  using PenaltyType = Penalty;

private:
  using FunctionSpaceType = typename GridFunctionType::FunctionSpaceType;

  using DomainFieldType = typename FunctionSpaceType::DomainFieldType;
  using RangeFieldType = typename FunctionSpaceType::RangeFieldType;

  using RangeType = typename FunctionSpaceType::RangeType;

  using GridPartType = typename GridFunctionType::GridPartType;
  using EntityType = typename GridPartType::template Codim< 0 >::EntityType;
  using IntersectionType = typename GridPartType::IntersectionType;

  using LocalDofVectorType = Dune::DynamicVector< RangeFieldType >;
  template< class Entity >
  using LocalL2Projection = __PoissonErrorIndicator::LocalL2Projection< Entity, RangeType >;
  template< class Entity >
  using ProjectedLocalFunctionType = Dune::Fem::LocalFunction< typename LocalL2Projection< Entity >::BasisFunctionSetType, LocalDofVectorType >;

public:
  /** \name Construction
   *  \{
   */

  PoissonErrorEstimate ( const GridFunctionType &f, const BoundaryDataType &g,
                         const PenaltyType &penalty )
    : f_( f ),
      g_( g ),
      penalty_( penalty )
  {}

  PoissonErrorEstimate ( const ThisType & ) = default;

  PoissonErrorEstimate ( ThisType && ) = default;

  /** \} */

  /** \name Assignment
   *  \{
   */

  ThisType &operator= ( const ThisType & ) = default;

  ThisType &operator= ( ThisType && ) = default;

  /** \} */

  /** \name Public member methods
   *  \{
   */

  template< class DiscreteFunction, class Mapper, class Array >
  void operator() ( const DiscreteFunction &uh, const Mapper &mapper, Array &array ) const;

  /** \} */

private:
  RangeFieldType gamma ( const IntersectionType &intersection ) const
  {
    return penalty_.get().gamma( intersection );
  }

  const GridFunctionType &f () const { return f_.get(); }

  const BoundaryDataType &g () const { return g_.get(); }

  template< class F, class Uh >
  static __PoissonErrorIndicator::Residual< F, Uh > residual ( const F &f, const Uh &uh )
  {
    return __PoissonErrorIndicator::Residual< F, Uh >( f, uh );
  }

  template< class Entity >
  static LocalL2Projection< Entity > l2Projection ( const Entity &entity, int order )
  {
    return LocalL2Projection< Entity >( entity, order );
  }

  template< class LocalFunction, class Entity = typename LocalFunction::EntityType >
  static void project ( const LocalFunction &localFunction, int order, ProjectedLocalFunctionType< Entity > &projection )
  {
    const auto l2Projection = ThisType::l2Projection( localFunction.entity(), order );
    projection.init( l2Projection.basisFunctionSet() );
    l2Projection( localFunction, projection );
  }

  template< class LocalFunction >
  static RangeFieldType norm2 ( LocalFunction &localFunction )
  {
    return Dune::Fem::LocalL2Norm::norm2( localFunction );
  }

  template< class Entity >
  DomainFieldType diameter ( const Entity &entity ) const
  {
    return ::diameter( entity );
  }

  template< std::size_t n, class Field >
  static Field power ( Field x )
  {
    Field value = static_cast< Field >( 1 );
    for( std::size_t i = 0u; i < n; ++i )
      value *= x;
    return value;
  }

  std::reference_wrapper< const GridFunctionType > f_;
  std::reference_wrapper< const BoundaryDataType > g_;
  std::reference_wrapper< const PenaltyType > penalty_;

  mutable ProjectedLocalFunctionType< EntityType > localFunction_;
  mutable ProjectedLocalFunctionType< IntersectionType > trace_;
};



// PoissonErrorEstimate::operator()
// --------------------------------

template< class GridFunction, class BoundaryData, class Penalty >
template< class DiscreteFunction, class Mapper, class Array >
inline void PoissonErrorEstimate< GridFunction, BoundaryData, Penalty >
  ::operator() ( const DiscreteFunction &uh, const Mapper &mapper, Array &array ) const
{
  assert( static_cast< std::size_t >( array.size() )
      == static_cast< std::size_t >( mapper.size() ) );
  std::fill( array.begin(), array.end(), 0 );

  const GridPartType &gridPart = uh.gridPart();
  assert( &gridPart == &f().gridPart() );

  auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
  auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
  for( ; it != end; ++it )
  {
    const EntityType &entity = *it;
    using LocalFunctionType = typename DiscreteFunction::LocalFunctionType;
    const LocalFunctionType inside = uh.localFunction( entity );

    const auto h_inside = diameter( entity );
    const int p_inside = inside.order();
    assert( p_inside > 0 );

    const auto residual = this->residual( f().localFunction( entity ), inside );
    project( residual, p_inside-1, localFunction_ );
    array[ mapper.index( entity ) ]
      += power< 2 >( h_inside / p_inside )*norm2( localFunction_ );

    auto iit = gridPart.ibegin( entity );
    auto iend = gridPart.iend( entity );
    for( ; iit != iend; ++iit )
    {
      const IntersectionType &intersection = *iit;
      if( intersection.neighbor() )
      {
        EntityType neighbor = intersection.outside();
        if( neighbor.partitionType() == Dune::InteriorEntity
              && mapper.index( entity ) < mapper.index( neighbor ) )
          continue;

        const LocalFunctionType outside = uh.localFunction( neighbor );

        const auto h_outside = diameter( neighbor );
        const int p_outside = outside.order();

        const auto normalDerivativeJump = __PoissonErrorIndicator::NormalDerivativeJump< IntersectionType, LocalFunctionType >( intersection, inside, outside );
        project( normalDerivativeJump, std::max( p_inside, p_outside )-1, trace_ );
        RangeFieldType value = norm2( trace_ );
        array[ mapper.index( entity ) ] += ( h_inside / p_inside ) * value;
        array[ mapper.index( neighbor ) ] += ( h_outside / p_outside ) * value;

        const auto gamma = ThisType::gamma( intersection );
        const auto jump = __PoissonErrorIndicator::Jump< IntersectionType, LocalFunctionType >( intersection, inside, outside );
        value = norm2( jump );
        array[ mapper.index( entity ) ]
          += ( power< 2 >( gamma ) * power< 3 >( p_inside ) / h_inside ) * value;
        array[ mapper.index( neighbor ) ]
          += ( power< 2 >( gamma ) * power< 3 >( p_outside ) / h_outside ) * value;
      }
      else if( intersection.boundary() )
      {
        const auto jump = __PoissonErrorIndicator::BoundaryTrace< typename BoundaryDataType::LocalFunctionType, LocalFunctionType >( g().localFunction( intersection ), inside );
        array[ mapper.index( entity ) ]
          += ( power< 2 >( gamma( intersection ) ) * power< 3 >( p_inside ) / h_inside ) * norm2( jump );
      }
    }
  }

  for( auto &value : array )
    value = std::sqrt( value );
}

#endif // #ifndef EXAMPLES_POISSON_INDICATOR_HH
