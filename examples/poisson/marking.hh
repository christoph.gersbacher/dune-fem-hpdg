#ifndef EXAMPLES_POISSON_MARKING_HH
#define EXAMPLES_POISSON_MARKING_HH

#include <functional>
#include <tuple>
#include <type_traits>

#include <dune/common/bartonnackmanifcheck.hh>
#include <dune/common/ftraits.hh>
#include <dune/common/unused.hh>

#include <dune/fem/function/localfunction/average.hh>

/** \brief please doc me
 *
 *  \ingroup Marking
 */
enum class Flag
{
  Coarsen = -1,  //!< mark element for coarsening
  None = 0,      //!< no refinement/coarsening needed
  Refine = 1     //!< mark element for refinement
};



// MarkingStrategy
// ---------------

/** \brief please doc me
 *
 *  \tparam  GridPart  grid part type
 *  \tparam  Implementation  real implementation of this interface class
 *
 *  \ingroup Marking
 */
template< class GridPart, class Implementation >
class MarkingStrategy
{
public:
  /** \brief grid part type */
  using GridPartType = GridPart;
  /** \brief entity type */
  using EntityType = typename GridPartType::template Codim< 0 >::EntityType;

protected:
  MarkingStrategy () = default;

public:
  void initialize ()
  {
    CHECK_INTERFACE_IMPLEMENTATION( impl().initialize() );
    impl().initialize();
  }

  /** \brief please doc me */
  Flag operator() ( const EntityType &entity )
  {
    CHECK_INTERFACE_IMPLEMENTATION( impl()( entity ) );
    return impl()( entity );
  }

  void finalize ()
  {
    CHECK_INTERFACE_IMPLEMENTATION( impl().finalize() );
    impl().finalize();
  }

protected:
  Implementation &impl ()
  {
    return static_cast< const Implementation & >( *this );
  }
};



// GoalOrientedMarkingStrategy
// ---------------------------

/** \brief please doc me
 *
 *  \tparam  GridPart  grid part type
 *  \tparam  Function  local error indicators
 *
 *  \ingroup Marking
 */
template< class GridPart, class Function >
class GoalOrientedMarkingStrategy
  : public MarkingStrategy< GridPart, GoalOrientedMarkingStrategy< GridPart, Function > >
{
  using BaseType = MarkingStrategy< GridPart, GoalOrientedMarkingStrategy< GridPart, Function > >;

public:
  /** \copydoc MarkingStrategy::GridPartType */
  using GridPartType = typename BaseType::GridPartType;
  /** \copydoc MarkingStrategy::EntityType */
  using EntityType = typename BaseType::EntityType;
  /** \brief field type */
  using FieldType = typename std::result_of< Function(EntityType) >::type;

  /** \name Construction
   *  \{
   */

  GoalOrientedMarkingStrategy ( const GridPartType &gridPart, const Function &eta, FieldType goal )
    : gridPart_( gridPart ),
      eta_( eta ),
      goal_( goal )
  {}

  GoalOrientedMarkingStrategy ( const GoalOrientedMarkingStrategy & ) = default;

  GoalOrientedMarkingStrategy &operator= ( const GoalOrientedMarkingStrategy & ) = default;

  /** \} */

  /** \name Public member methods
   *  \{
   */

  void initialize ()
  {
    const GridPartType &gridPart = gridPart_.get();
    std::size_t elements = 0u;
    auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
    auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
    for( ; it != end; ++it )
      ++elements;
    elements = gridPart.comm().sum( elements );
    threshold_ = goal_/elements;
  }

  Flag operator() ( const EntityType &entity )
  {
    const auto eta = eta_( entity );
    if( eta > threshold_ )
      return Flag::Refine;
    if( eta < 0.25*threshold_ )
      return Flag::Coarsen;
    return Flag::None;
  }

  void finalize () {}

  /** \} */

private:
  std::reference_wrapper< const GridPartType > gridPart_;
  Function eta_;
  FieldType goal_, threshold_;
};



// mark
// ----

template< class Grid, class ManagedArray,  class Function >
void mark ( Grid &grid, ManagedArray &p, Function function )
{
  for( const auto &element : Dune::elements( grid.leafGridView(), Dune::Partitions::interiorBorder ) )
  {
    // get refinement flag and estimated polynomial degree
    Flag what;
    int q;
    std::tie( what, q ) = function( element );

    // mark space/grid for hp-refinement/coarsening
    switch( what )
    {
      case Flag::None:
        break;
      case Flag::Coarsen:
        if( element.hasFather() )
          grid.mark( -1, element );
        else if( p[ element ] > 3 )
          p[ element ] -= 1;
        break;
      case Flag::Refine:
        if( p[ element ] < q )
          p[ element ] += 1;
        else
          grid.mark( 1, element );
    }
  }
}

#endif // #ifndef EXAMPLES_POISSON_MARKING_HH
