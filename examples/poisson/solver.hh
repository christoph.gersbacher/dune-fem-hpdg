#ifndef EXAMPLES_POISSON_SOLVER_HH
#define EXAMPLES_POISSON_SOLVER_HH

#include <cassert>
#include <cstddef>

#include <algorithm>
#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/outerproduct.hh>
#include <dune/common/std/utility.hh>

#if HAVE_DUNE_ISTL
#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/operator/linear/istloperator.hh>
#include <dune/fem/solver/istlsolver.hh>
#endif // #if HAVE_DUNE_ISTL

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>
#include <dune/fem/operator/common/stencil.hh>
#include <dune/fem/operator/linear/spoperator.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/elementquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/solver/cginverseoperator.hh>
#include <dune/fem/solver/oemsolver.hh>

#include "penalty.hh"

// Tags
// ---

struct USE_FEM {};
struct USE_ISTL {};
struct USE_OEM {};



// PoissonSolverTraits
// -------------------

template< class DiscreteFunctionSpace,
          class Tag = typename std::conditional< HAVE_DUNE_ISTL, USE_ISTL, USE_OEM >::type >
class PoissonSolverTraits;

template< class DiscreteFunctionSpace >
class PoissonSolverTraits< DiscreteFunctionSpace, USE_FEM >
{
public:
  using DiscreteFunctionType = Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpace >;
  using LinearOperatorType = Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType >;
  using LinearInverseOperatorType = Dune::Fem::Solver::CGInverseOperator< DiscreteFunctionType >;
};

#if HAVE_DUNE_ISTL

template< class DiscreteFunctionSpace >
class PoissonSolverTraits< DiscreteFunctionSpace, USE_ISTL >
{
public:
  using DiscreteFunctionType = Dune::Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpace >;
  using LinearOperatorType = Dune::Fem::ISTLLinearOperator< DiscreteFunctionType, DiscreteFunctionType >;
  using LinearInverseOperatorType = Dune::Fem::ISTLCGOp< DiscreteFunctionType, LinearOperatorType >;
};

#endif // #if HAVE_DUNE_ISTL

template< class DiscreteFunctionSpace >
class PoissonSolverTraits< DiscreteFunctionSpace, USE_OEM >
{
public:
  using DiscreteFunctionType = Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpace >;
  using LinearOperatorType = Dune::Fem::SparseRowLinearOperator< DiscreteFunctionType, DiscreteFunctionType >;
  using LinearInverseOperatorType = Dune::Fem::OEMCGOp< DiscreteFunctionType, LinearOperatorType >;
};



namespace __PoissonSolver
{

  // Operator
  // --------

  template< class JacobianOperator >
  class Operator final
    : public Dune::Fem::DifferentiableOperator< JacobianOperator >
  {
    using ThisType = Operator< JacobianOperator >;
    using BaseType = Dune::Fem::DifferentiableOperator< JacobianOperator >;

  public:
    /** \brief jacobian operator type */
    using JacobianOperatorType = typename BaseType::JacobianOperatorType;

    /** \brief domain function type */
    using DomainFunctionType = typename BaseType::DomainFunctionType;
    /** \brief range function type */
    using RangeFunctionType = typename BaseType::RangeFunctionType;

    /** \brief range field type */
    using RangeFieldType = typename BaseType::RangeFieldType;

  private:
    static_assert( std::is_same< typename JacobianOperator::DomainSpaceType, typename JacobianOperator::RangeSpaceType >::value,
                   "Domain and range function space must be identical" );

    using DiscreteFunctionSpaceType = typename JacobianOperator::DomainSpaceType;
    using GridPartType = typename DiscreteFunctionSpaceType::GridPartType;
    using EntityType = typename DiscreteFunctionSpaceType::EntityType;
    using IntersectionType = typename GridPartType::IntersectionType;

    using LocalMatrixType = typename JacobianOperatorType::LocalMatrixType;
    using BasisFunctionSetType = typename LocalMatrixType::DomainBasisFunctionSetType;

    using RangeType = typename BasisFunctionSetType::RangeType;
    using JacobianRangeType = typename BasisFunctionSetType::JacobianRangeType;

    template< int codim >
    using QuadratureType = typename std::conditional< codim == 0,
          Dune::Fem::CachingQuadrature< GridPartType, codim >,
          Dune::Fem::ElementQuadrature< GridPartType, codim >
            >::type;

    enum { Inside = 0u, Outside = 1u };

  public:
    /** \brief penalty type */
    using PenaltyType = Penalty< DiscreteFunctionSpaceType >;

    /** \name Construction
     *  \{
     */

    explicit Operator ( const PenaltyType &penalty )
      : penalty_( penalty )
    {}

    Operator ( const ThisType & ) = default;

    Operator ( ThisType && ) = default;

    /** \} */

    /** \name Assignment
     *  \{
     */

    ThisType &operator= ( const ThisType & ) = default;

    ThisType &operator= ( ThisType && ) = default;

    /** \} */

    /** \name Public member methods
     *  \{
     */

    void operator() ( const DomainFunctionType &u, RangeFunctionType &v ) const
    {
      DUNE_THROW( Dune::NotImplemented, "Operator::operator() not implemented yet" );
    }

    void jacobian ( const DomainFunctionType &u, JacobianOperatorType &jacobian ) const;

    /** \} */

  private:
    void axpy ( const EntityType &entity, LocalMatrixType &block ) const;

    template< bool conforming >
    void axpy ( const IntersectionType &intersection, std::pair< LocalMatrixType &, LocalMatrixType & > blocks ) const;

    void axpy ( const IntersectionType &intersection, LocalMatrixType &block ) const;

    template< class Point >
    static void evaluateAll ( const BasisFunctionSetType &basisFunctionSet, const Point &x,
                              std::vector< RangeType > &values,
                              std::vector< JacobianRangeType > &jacobians )
    {
      assert( values.size() >= basisFunctionSet.size() );
      basisFunctionSet.evaluateAll( x, values );
      assert( jacobians.size() >= basisFunctionSet.size() );
      basisFunctionSet.jacobianAll( x, jacobians );
    }

    static void ignore( ... ) {}

    template< class... Args >
    static void resize ( std::size_t size, Args &... args )
    {
      resize( size, std::tie( args... ), Dune::Std::index_sequence_for< Args... >() );
    }

    template< class... Args, std::size_t... Ints >
    static void resize ( std::size_t size, std::tuple< Args &... > tuple, Dune::Std::index_sequence< Ints... > )
    {
      ignore( ( std::get< Ints >( tuple ).resize( size ), Ints )... );
    }

    const PenaltyType &penalty () const { return penalty_.get(); }

    std::reference_wrapper< const PenaltyType > penalty_;
    mutable std::vector< RangeType > values_[ 2 ];
    mutable std::vector< JacobianRangeType > jacobians_[ 2 ];
  };



  template< class JacobianOperator >
  inline void Operator< JacobianOperator >
    ::jacobian ( const DomainFunctionType &u, JacobianOperatorType &jacobian ) const
  {
    const DiscreteFunctionSpaceType &space = u.space();
    const GridPartType &gridPart = space.gridPart();

    using StencilType
      = Dune::Fem::DiagonalAndNeighborStencil< typename DomainFunctionType::DiscreteFunctionSpaceType, typename RangeFunctionType::DiscreteFunctionSpaceType >;
    StencilType stencil( space, space );

    jacobian.reserve( stencil );
    jacobian.clear();

    auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
    auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
    for( ; it != end; ++it )
    {
      const EntityType &entity = *it;
      LocalMatrixType diagonal = jacobian.localMatrix( entity, entity );
      axpy( entity, diagonal );

      auto iit = gridPart.ibegin( entity );
      auto iend = gridPart.iend( entity );
      for( ; iit != iend; ++iit )
      {
        const IntersectionType &intersection = *iit;
        if( intersection.neighbor() )
        {
          EntityType outside = intersection.outside();
          LocalMatrixType neighbor = jacobian.localMatrix( outside, entity );

          if( intersection.conforming() )
            axpy< true >( intersection, std::make_pair( std::ref( diagonal ), std::ref( neighbor ) ) );
          else
            axpy< false >( intersection, std::make_pair( std::ref( diagonal ), std::ref( neighbor ) ) );
        }
        else if( intersection.boundary() )
          axpy( intersection, diagonal );
      }
    }
    jacobian.communicate();
  }



  template< class JacobianOperator >
  inline void Operator< JacobianOperator >
    ::axpy ( const EntityType &entity, LocalMatrixType &block ) const
  {
    BasisFunctionSetType basisFunctionSet = block.domainBasisFunctionSet();

    const std::size_t size = basisFunctionSet.size();
    auto &jacobians = jacobians_[ Inside ];
    resize( size, jacobians );

    auto geometry = entity.geometry();

    const int order = 2*basisFunctionSet.order() + 1;
    QuadratureType< 0 > quadrature( entity, order );
    const std::size_t nop = quadrature.nop();
    for( std::size_t qp = 0u; qp < nop; ++qp )
    {
      const auto &x = quadrature.point( qp );
      const auto weight = quadrature.weight( qp )*geometry.integrationElement( x );

      basisFunctionSet.jacobianAll( quadrature[ qp ], jacobians );
      for( std::size_t i = 0; i < size; ++i )
        block.column( i ).axpy( jacobians, jacobians[ i ], weight );
    }
  }



  template< class JacobianOperator >
  template< bool conforming >
  inline void Operator< JacobianOperator >
    ::axpy ( const IntersectionType &intersection, std::pair< LocalMatrixType &, LocalMatrixType & > blocks ) const
  {
    auto basisFunctionSets = std::make_pair( std::ref( blocks.first.domainBasisFunctionSet() ), std::ref( blocks.second.domainBasisFunctionSet() ) );

    auto sizes = std::make_pair( basisFunctionSets.first.size(), basisFunctionSets.second.size() );
    resize( sizes.first, values_[ Inside ], jacobians_[ Inside ] );
    resize( sizes.second, values_[ Outside ], jacobians_[ Outside ] );

    auto geometry = intersection.geometry();
    const double sigma = penalty().sigma( intersection, PenaltyType::Intersection::Interior );

    using QuadraturesType = Dune::Fem::IntersectionQuadrature< QuadratureType< 1 >, conforming >;
    const int order = basisFunctionSets.first.order() + basisFunctionSets.second.order() + 1;
    QuadraturesType quadratures( blocks.first.domainSpace().gridPart(), intersection, order );

    const auto &quadrature = quadratures.inside();
    const std::size_t nop = quadrature.nop();
    for( std::size_t qp = 0u; qp < nop; ++qp )
    {
      const auto &x = quadrature.localPoint( qp );
      const auto weight = quadrature.weight( qp )*geometry.integrationElement( x );

      const auto normal = intersection.unitOuterNormal( x );

      RangeType valueFactor;
      JacobianRangeType jacobianFactor;

      evaluateAll( basisFunctionSets.first, quadratures.inside()[ qp ], values_[ Inside ], jacobians_[ Inside ] );
      for( std::size_t i = 0; i < sizes.first; ++i )
      {
        valueFactor = 0;
        jacobians_[ Inside ][ i ].usmv( -0.5, normal, valueFactor );
        valueFactor.axpy( sigma, values_[ Inside ][ i ] );

        jacobianFactor = outerProduct( values_[ Inside ][ i ], normal );
        jacobianFactor *= -0.5;

        blocks.first.column( i ).axpy( values_[ Inside ], jacobians_[ Inside ], valueFactor, jacobianFactor, weight );
      }

      evaluateAll( basisFunctionSets.second, quadratures.outside()[ qp ], values_[ Outside ], jacobians_[ Outside ] );
      for( std::size_t i = 0; i < sizes.second; ++i )
      {
        valueFactor = 0;
        jacobians_[ Outside ][ i ].usmv( -0.5, normal, valueFactor );
        valueFactor.axpy( -sigma, values_[ Outside ][ i ] );

        jacobianFactor = outerProduct( values_[ Outside ][ i ], normal );
        jacobianFactor *= 0.5;

        blocks.second.column( i ).axpy( values_[ Inside ], jacobians_[ Inside ], valueFactor, jacobianFactor, weight );
      }
    }
  }



  template< class JacobianOperator >
  inline void Operator< JacobianOperator >
    ::axpy ( const IntersectionType &intersection, LocalMatrixType &block ) const
  {
    BasisFunctionSetType basisFunctionSet = block.domainBasisFunctionSet();

    const std::size_t size = basisFunctionSet.size();
    auto &values = values_[ Inside ];
    auto &jacobians = jacobians_[ Inside ];
    resize( size, values, jacobians );

    auto geometry = intersection.geometry();
    const double sigma = penalty().sigma( intersection, PenaltyType::Intersection::Boundary );

    const int order = 2*basisFunctionSet.order() + 1;
    QuadratureType< 1 > quadrature( block.domainSpace().gridPart(), intersection, order, QuadratureType< 1 >::INSIDE );
    const std::size_t nop = quadrature.nop();
    for( std::size_t qp = 0u; qp < nop; ++qp )
    {
      const auto &x = quadrature.localPoint( qp );
      const auto weight = quadrature.weight( qp )*geometry.integrationElement( x );

      const auto normal = intersection.unitOuterNormal( x );

      RangeType valueFactor;
      JacobianRangeType jacobianFactor;

      evaluateAll( basisFunctionSet, quadrature[ qp ], values, jacobians );
      for( std::size_t i = 0; i < size; ++i )
      {
        valueFactor = 0;
        jacobians[ i ].usmv( -1., normal, valueFactor );
        valueFactor.axpy( sigma, values[ i ] );

        jacobianFactor = outerProduct( values[ i ], normal );
        jacobianFactor *= -1;

        block.column( i ).axpy( values, jacobians, valueFactor, jacobianFactor, weight );
      }
    }
  }

} // namespace __PoissonSolver



// PoissonSolver
// -------------

template< class Traits >
class PoissonSolver final
{
  using ThisType = PoissonSolver< Traits >;

  using LinearOperatorType = typename Traits::LinearOperatorType;
  using DifferentiableOperatorType = __PoissonSolver::Operator< LinearOperatorType >;
  using LinearInverseOperatorType = typename Traits::LinearInverseOperatorType;

public:
  /** \brief discrete function type */
  using DiscreteFunctionType = typename LinearOperatorType::DomainFunctionType;
  /** \brief discrete function space type */
  using DiscreteFunctionSpaceType = typename DiscreteFunctionType::DiscreteFunctionSpaceType;

private:
  using GridPartType = typename DiscreteFunctionType::GridPartType;
  using IntersectionType = typename GridPartType::IntersectionType;
  using EntityType = typename IntersectionType::Entity;

  using RangeFieldType = typename DiscreteFunctionType::RangeFieldType;

  using RangeType = typename DiscreteFunctionType::RangeType;
  using JacobianRangeType = typename DiscreteFunctionType::JacobianRangeType;

public:
  /** \brief penalty type */
  using PenaltyType = Penalty< DiscreteFunctionSpaceType >;

  /** \name Construction
   *  \{
   */

  PoissonSolver ( const DiscreteFunctionSpaceType &space, const PenaltyType &penalty )
    : fh_( "fh", space ),
      linearOperator_( "assempled elliptic operator", space, space ),
      penalty_( penalty ),
      tolerance_( Dune::Fem::Parameter::getValue< RangeFieldType >( "ip.solver.tolerance" ) )
  {}

  PoissonSolver ( const ThisType & ) = default;

  PoissonSolver ( ThisType && ) = default;

  /** \} */

  /** \name Assignment
   *  \{
   */

  ThisType &operator= ( const ThisType & ) = default;

  ThisType &operator= ( ThisType && ) = default;

  /** \} */

  /** \name Public member functions
   *  \{
   */

  template< class F, class G >
  void operator() ( const F &f, const G &g, DiscreteFunctionType &uh ) const
  {
    assert( &uh.space() == &fh_.space() );

    // assemble linear operator
    DifferentiableOperatorType differentiableOperator( penalty_ );
    differentiableOperator.jacobian( uh, linearOperator_ );

    // assemble right hand side
    assemble( f, g, fh_ );

    // solve system and communicate
    LinearInverseOperatorType solve( linearOperator_, tolerance_, tolerance_ );
    solve( fh_, uh );

    uh.communicate();
  }

  /** \} */

private:
  template< class F, class G >
  void assemble ( const F &f, const G &g, DiscreteFunctionType &fh ) const;

  const PenaltyType &penalty () const { return penalty_.get(); }

  mutable DiscreteFunctionType fh_;
  mutable LinearOperatorType linearOperator_;
  std::reference_wrapper< const PenaltyType > penalty_;
  RangeFieldType tolerance_;
};



// Implementation of PoissonSolver
// -------------------------------

template< class Traits >
template< class F, class G >
inline void PoissonSolver< Traits >
  ::assemble ( const F &f, const G &g, DiscreteFunctionType &fh ) const
{
  // clear right hand side
  fh.clear();

  // traverse grid part
  const GridPartType &gridPart = fh.gridPart();
  auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
  auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
  for( ; it != end; ++it )
  {
    // get geometry
    const EntityType &entity = *it;
    const auto geometry = entity.geometry();

    // get local functions
    const auto f_local = f.localFunction( entity );
    auto fh_local = fh.localFunction( entity );

    // quadrature loop
    int order = f_local.order() + fh_local.order();
    Dune::Fem::CachingQuadrature< GridPartType, 0 > quadrature( entity, order );
    const std::size_t nop = quadrature.nop();
    for( std::size_t qp = 0u; qp < nop; ++qp )
    {
      const auto &x = quadrature.point( qp );
      const auto weight = quadrature.weight( qp ) * geometry.integrationElement( x );

      RangeType value;
      f_local.evaluate( quadrature[ qp ], value );
      value *= weight;

      fh_local.axpy( quadrature[ qp ], value );
    }

    auto iit = gridPart.ibegin( entity );
    auto iend = gridPart.iend( entity );
    for( ; iit != iend; ++iit )
    {
      const IntersectionType &intersection = *iit;
      if( !intersection.boundary() )
        continue;

      const auto sigma = penalty().sigma( intersection, PenaltyType::Intersection::Boundary );

      const auto geometry = intersection.geometry();

      auto g_local = g.localFunction( intersection );
      const int order = g_local.order() + fh_local.order();

      using QuadratureType = Dune::Fem::ElementQuadrature< GridPartType, 1 >;
      QuadratureType quadrature( gridPart, intersection, order, QuadratureType::INSIDE );
      const std::size_t nop = quadrature.nop();
      for( std::size_t qp = 0u; qp < nop; ++qp )
      {
        const auto &x = quadrature.localPoint( qp );
        const auto weight = quadrature.weight( qp ) * geometry.integrationElement( x );

        RangeType value;
        g_local.evaluate( x, value );

        auto normal = intersection.unitOuterNormal( x );
        normal *= -1;
        JacobianRangeType jacobian = outerProduct( value, normal );

        value *= sigma * weight;
        jacobian *= weight;

        fh_local.axpy( quadrature[ qp ], value, jacobian );
      }
    }
  }

  // final communication
  fh.communicate();
}

#endif // #ifndef EXAMPLES_POISSON_SOLVER_HH
