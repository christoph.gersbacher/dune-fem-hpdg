#include <config.h>

#include <cmath>

#include <algorithm>
#include <iostream>
#include <tuple>
#include <utility>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/unused.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/io/file/dgfparser/dgfparser.hh>
#include <dune/grid/io/file/dgfparser/gridptr.hh>

#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>

#include <dune/fem/function/grid/simple.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/hpdg/space/common/adaptationmanager.hh>
#include <dune/fem/hpdg/space/common/dataprojection.hh>
#include <dune/fem/hpdg/space/discontinuousgalerkin/orthogonal.hh>
#include <dune/fem/hpdg/utility/managedarray.hh>
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/restrictprolongtuple.hh>

#include "energynorm.hh"
#include "gridfunction.hh"
#include "indicator.hh"
#include "l2error.hh"
#include "marking.hh"
#include "penalty.hh"
#include "problemdata.hh"
#include "reentrantcorner.hh"
#include "regularity.hh"
#include "solver.hh"

int main ( int argc, char **argv )
try
{
  // initialize MPI
  Dune::Fem::MPIManager::initialize( argc, argv );

  if( argc < 2 )
  {
    std::cerr << "Usage" << argv[ 0 ] << " <dgf file>" << std::endl;
    return 0;
  }

  // read parameters
  Dune::Fem::Parameter::append( argc, argv );
  Dune::Fem::Parameter::appendDGF( argv[ 1 ] );

  // create grid
  using GridType
#if defined USE_ALUGRID_CUBE
    = Dune::ALUGrid< 2, 2, Dune::cube, Dune::nonconforming >;
#elif defined USE_ALUGRID_SIMPLEX
    = Dune::ALUGrid< 2, 2, Dune::simplex, Dune::nonconforming >;
#endif
  Dune::GridPtr< GridType > grid( argv[ 1 ] );
  grid->loadBalance();

  // initial refinement
  const auto refineStepsForHalf = Dune::DGFGridInfo< GridType >::refineStepsForHalf();
  const auto count = Dune::Fem::Parameter::getValue< int >( "ip.refinement.initial", 0 );
  Dune::Fem::GlobalRefine::apply( *grid, count*refineStepsForHalf );

  // get mapper
  using MapperType = Dune::LeafMultipleCodimMultipleGeomTypeMapper< GridType, Dune::MCMGElementLayout >;
  MapperType mapper( *grid );

  // create grid part
  using GridPartType = Dune::Fem::AdaptiveLeafGridPart< GridType >;
  GridPartType gridPart( *grid );

  // create grid functions
  using FunctionSpaceType = Dune::Fem::FunctionSpace< GridPartType::ctype, double, GridPartType::dimensionworld, 1 >;
  using ProblemType = ReentrantCorner< FunctionSpaceType >;
  auto u = gridFunction( "u", ProblemType(), gridPart, ORDER );
  auto f = gridFunction( "f", RightHandSide< ProblemType >(), gridPart, ORDER );
  auto g = BoundaryData< ProblemType, GridPartType::IntersectionType >( ProblemType(), ORDER );

  // create vector of local polynomial degrees
  using ManagedArrayType = Dune::Fem::hpDG::ManagedArray< GridPartType >;
  ManagedArrayType p( gridPart, 3 );

  // create discrete function space
  using DiscreteFunctionSpaceType = Dune::Fem::hpDG::OrthogonalDiscontinuousGalerkinSpace< FunctionSpaceType, GridPartType, ORDER >;
  using ElementType = DiscreteFunctionSpaceType::EntityType;
  DiscreteFunctionSpaceType space( gridPart, [&p]( const ElementType &element ){ return p[ element ]; } );

  // create solver
  using Traits = PoissonSolverTraits< DiscreteFunctionSpaceType >;
  using SolverType = PoissonSolver< Traits >;
  Penalty< DiscreteFunctionSpaceType > penalty( space );
  SolverType solver( space, penalty );

  // create discrete function
  using DiscreteFunctionType = SolverType::DiscreteFunctionType;
  DiscreteFunctionType uh( "uh", space );
  uh.clear();

  // create error estimator
  using ErrorEstimatorType = PoissonErrorEstimate< decltype( f ), decltype( g ), decltype( penalty ) >;
  ErrorEstimatorType errorEstimator( f, g, penalty );
  std::vector< double > eta;

  // create regularity indicator type
  using RegularityIndicatorType = Prior2P< DiscreteFunctionType >;
  RegularityIndicatorType regularityIndicator;

  // create marking strategy
  const auto goal = Dune::Fem::Parameter::getValue< double >( "ip.error.goal", 1e-6 );
  auto function = [&mapper, &eta]( const ElementType &element )
  {
    return eta[ mapper.index( element ) ];
  };
  GoalOrientedMarkingStrategy< GridPartType, decltype( function ) > strategy( gridPart, function, goal );

  // create adaptation managers
  using RestrictProlongType
    = Dune::Fem::RestrictProlongDefaultTuple< ManagedArrayType, DiscreteFunctionType >;
  RestrictProlongType restrictProlong( p, uh );
  Dune::Fem::AdaptationManager< GridType, RestrictProlongType > hAdaptManager( *grid, restrictProlong );
  using DataProjectionType
    = Dune::Fem::hpDG::DefaultDataProjectionTuple< DiscreteFunctionType >;
  Dune::Fem::hpDG::AdaptationManager< DiscreteFunctionSpaceType, DataProjectionType > pAdaptManager( space, DataProjectionType( uh ) );

  // file I/O
  using LocalCoordinateType = ElementType::Geometry::LocalCoordinate;
  auto ph = Dune::Fem::simpleGridFunction( "p", gridPart, [&space]( const ElementType &element, const LocalCoordinateType &) -> Dune::FieldVector< double, 1 > { return space.order( element ); }, 0 );
  auto tuple = std::make_tuple( &uh, &ph );
  Dune::Fem::DataOutput< GridType, decltype( tuple ) > output( *grid, tuple );

  // energy norm
  using EnergyNormType = EnergyNorm< DiscreteFunctionSpaceType >;
  EnergyNormType energyNorm( penalty );

  const auto steps = Dune::Fem::Parameter::getValue< int >( "ip.refinement.steps", 0 );
  std::cout << "step"
            << "\tno. elements"
            << "\tno. dofs"
            << "\t||u - uh||_L2"
            << "\tEOC"
            << "\t||u - uh||_DG"
            << "\tEOC"
            << "\t(\\sum \\eta^2)^{1/2}"
            << "\teff. index"
            << std::endl;

  // compute EOC in number of DOFs
  const int L2 = 0, DG = 1;
  double error[ 2 ][ 2 ] = {{0.,0.}, {0.,0.}};
  int dofs[ 2 ] = {0, 0};

  for( int step = 0; step < steps; ++step )
  {
    // solve Poisson's equation
    if( Dune::Fem::Parameter::verbose() )
      std::cerr << "solving Poisson's equation... ";
    solver( f, g, uh );
    if( Dune::Fem::Parameter::verbose() )
      std::cerr << "done" << std::endl;

    if( Dune::Fem::Parameter::verbose() )
      std::cerr << "computing local error indicators... ";
    eta.resize( mapper.size() );
    errorEstimator( uh, mapper, eta );
    if( Dune::Fem::Parameter::verbose() )
      std::cerr << "done" << std::endl;

    // compute total numbers of DOFs and elements and energy error estimate
    int elements = dofs[ 0 ] = 0;
    double Sigma = 0.;
    auto it = gridPart.begin< 0, Dune::InteriorBorder_Partition >();
    auto end = gridPart.end< 0, Dune::InteriorBorder_Partition >();
    for( ; it != end; ++it )
    {
      const ElementType &element = *it;
      dofs[ 0 ] += FunctionSpaceType::dimRange*space.blockMapper().numDofs( element );
      ++elements;
      Sigma += eta[ mapper.index( element ) ]*eta[ mapper.index( element ) ];
    }
    dofs[ 0 ] = grid->comm().sum( dofs[ 0 ] );
    elements = grid->comm().sum( elements );
    Sigma = grid->comm().sum( Sigma );
    Sigma = std::sqrt( Sigma );

    // compute errors
    error[ L2 ][ 0 ] = ::l2Error( u, uh );
    error[ DG ][ 0 ] = energyNorm.distance( u, uh );

    // print informations
    if( Dune::Fem::Parameter::verbose() )
    {
      using std::log;
      using std::pow;

      std::string eoc[ 2 ];
      const double denominator
        = log(pow( static_cast< double >( dofs[ 1 ] )/static_cast< double >( dofs[ 0 ] ), 1./GridPartType::dimension ));
      eoc[ L2 ] = step ? std::to_string( -log( error[ L2 ][ 1 ]/ error[ L2 ][ 0 ] )/denominator ) : "---";
      eoc[ DG ] = step ? std::to_string( -log( error[ DG ][ 1 ]/ error[ DG ][ 0 ] )/denominator ) : "---";

      std::cout << step
                << "\t" << elements
                << "\t" << dofs[ 0 ]
                << "\t" << error[ L2 ][ 0 ]
                << "\t" << eoc[ L2 ]
                << "\t" << error[ DG ][ 0 ]
                << "\t" << eoc[ DG ]
                << "\t" << Sigma
                << "\t" << Sigma/error[ DG ][ 0 ]
                << std::endl;
    }
    error[ L2 ][ 1 ] = error[ L2 ][ 0 ];
    error[ DG ][ 1 ] = error[ DG ][ 0 ];
    dofs[ 1 ] = dofs[ 0 ];

    // file I/O
    output.write();

    // adapt grid
    if( step < steps-1 )
    {
      // mark elements and modify local polynomial degrees
      strategy.initialize();
      auto function = [&strategy, &regularityIndicator, &uh]( const ElementType &element )
        -> std::pair< Flag, int >
      {
        Flag flag = strategy( element );
        if( flag == Flag::None )
          return std::make_pair( flag, 0 );

        std::pair< int, bool > estimate
          = regularityIndicator( uh.localFunction( element ) );
        return std::make_pair( flag, estimate.second ? estimate.first : 0 );
      };
      mark( *grid, p, function );
      strategy.finalize();

      // adapt grid
      hAdaptManager.adapt();
      mapper.update();

      // adapt space and project discrete solution
      auto it = gridPart.begin< 0, Dune::InteriorBorder_Partition >();
      auto end = gridPart.end< 0, Dune::InteriorBorder_Partition >();
      for( ; it != end; ++it )
      {
        const ElementType &element = *it;
        space.mark( std::min( p[ element ], space.order() ), element );
      }
      pAdaptManager.adapt();
    }
  }

  return 0;
}
catch( Dune::Exception &exception )
{
  std::cerr << exception << std::endl;
  return 1;
}
catch( std::exception &exception )
{
  std::cerr << exception.what() << std::endl;
  return 1;
}
catch( ... )
{
  std::cerr << "Unknown exception thrown" << std::endl;
  return 1;
}
