#ifndef EXAMPLES_POISSON_REENTRANTCORNER_HH
#define EXAMPLES_POISSON_REENTRANTCORNER_HH

#include <cassert>
#include <cmath>

#include <complex>

#include <dune/fem/function/common/function.hh>

#include "problemdata.hh"

// ReentrantCorner
// ---------------

template< class FunctionSpace >
class ReentrantCorner final
  : public Dune::Fem::Function< FunctionSpace, ReentrantCorner< FunctionSpace > >
{
  using ThisType = ReentrantCorner< FunctionSpace >;
  using BaseType = Dune::Fem::Function< FunctionSpace, ReentrantCorner< FunctionSpace > >;

public:
  using RangeFieldType = typename BaseType::RangeFieldType;

  using DomainType = typename BaseType::DomainType;
  using RangeType = typename BaseType::RangeType;
  using JacobianRangeType = typename BaseType::JacobianRangeType;
  using HessianRangeType = typename BaseType::HessianRangeType;

private:
  static_assert( DomainType::dimension == 2, "Reentrant corner problem only implemented for domain dimension 2" );
  static_assert( RangeType::dimension == 1, "Reentrant corner problem only implemented for range dimension 1" );

public:
  ReentrantCorner ()
    : lambda_( 180./270. )
  {}

  void evaluate ( const DomainType &x, RangeType &value ) const
  {
    auto r2 = x.two_norm2();
    auto phi = argphi( x[ 0 ], x[ 1 ] );
    value = std::pow( r2, 0.5*lambda_ ) * std::sin( lambda_*phi );
  }

  void jacobian ( const DomainType &x, JacobianRangeType &jacobian ) const
  {
    RangeFieldType gradient[ 2 ] = { 0.0, 0.0 };

    auto r2 = x.two_norm2();
    auto phi = argphi( x[ 0 ], x[ 1 ] );
    auto r2dx = 2.*x[ 0 ];
    auto r2dy = 2.*x[ 1 ];
    auto phidx = -x[ 1 ]/r2;
    auto phidy = x[ 0 ]/r2;
    auto lambdaPow = ( 0.5*lambda_ )*pow( r2, 0.5*lambda_ - 1.);

    gradient[ 0 ] = lambdaPow*r2dx*std::sin( lambda_*phi )
                    + lambda_*std::cos( lambda_*phi )*phidx*pow( r2, 0.5*lambda_ );
    gradient[ 1 ] = lambdaPow*r2dy*std::sin( lambda_*phi )
                    + lambda_*std::cos( lambda_ * phi )*phidy*pow( r2, 0.5*lambda_ );

    assert( gradient[ 0 ] == gradient[ 0 ] );
    assert( gradient[ 1 ] == gradient[ 1 ] );

    for( int i = 0; i < DomainType::dimension; ++i )
      jacobian[ 0 ][ i ] = gradient[ i ];
  }

private:
  RangeFieldType argphi ( RangeFieldType x, RangeFieldType y ) const
  {
    RangeFieldType phi = std::arg( std::complex< RangeFieldType >( x, y ) );
    if( y < 0 )
      phi += 2.*M_PI;
    return phi;
  }

  RangeFieldType lambda_;
};



// RightHandSide
// -------------

template< class FunctionSpace >
class RightHandSide< ReentrantCorner< FunctionSpace > > final
  : public Dune::Fem::Function< FunctionSpace, RightHandSide< ReentrantCorner< FunctionSpace > > >
{
  using BaseType = Dune::Fem::Function< FunctionSpace, RightHandSide< ReentrantCorner< FunctionSpace > > >;

public:
  static void evaluate ( const typename BaseType::DomainType &x, typename BaseType::RangeType &value )
  {
    value = 0;
  }
};

#endif // #ifndef EXAMPLES_POISSON_REENTRANTCORNER_HH
