#ifndef EXAMPLES_POISSON_DIAMETER_HH
#define EXAMPLES_POISSON_DIAMETER_HH

#include <cmath>

#include <algorithm>
#include <functional>
#include <vector>

#include <dune/grid/common/entity.hh>
#include <dune/grid/common/intersection.hh>

namespace Dune
{

  // External forward declarations
  // -----------------------------

  template<int cd, int dim, class Grid, template< int, int, class > class Implementaton >
  class Entity;
  template< class Grid, class Implementation >
  class Intersection;

} // namespace Dune


#ifndef DOXYGEN

template< class Geometry, bool cached >
struct __CoordinateStorage;

template< class Geometry >
struct __CoordinateStorage< Geometry, false >
{
  explicit __CoordinateStorage ( const Geometry &geometry )
    : geometry_( geometry )
  {}

  std::size_t size () const { return geometry().corners(); }

  typename Geometry::GlobalCoordinate operator[] ( std::size_t i ) const
  {
    return geometry().corner( i );
  }

private:
  const Geometry &geometry () const { return  geometry_.get(); }

  std::reference_wrapper< const Geometry > geometry_;
};

template< class Geometry >
struct __CoordinateStorage< Geometry, true >
{
  explicit __CoordinateStorage ( const Geometry &geometry )
  {
    const std::size_t size = geometry.corners();
    corners_.reserve( size );
    for( std::size_t i = 0u; i < size; ++i )
      corners_.emplace_back( geometry.corner( i ) );
  }

  std::size_t size () const { return corners_.size(); }

  const typename Geometry::GlobalCoordinate &operator[] ( std::size_t i ) const
  {
    return corners_[ i ];
  }

private:
  std::vector< typename Geometry::GlobalCoordinate > corners_;
};



template< class Geometry, bool cached >
class __Diameter
{
public:
  using ctype = typename Geometry::ctype;
  using GlobalCoordinate = typename Geometry::GlobalCoordinate;

  explicit __Diameter ( const Geometry &geometry )
    : corners_( geometry )
  {}

  ctype compute () const
  {
    ctype diameter2 = static_cast< ctype >( 0 );

    GlobalCoordinate x;
    const std::size_t size = corners_.size();
    for( std::size_t i = 0u; i < size; ++i )
    {
      x = corners_[ i ];
      for( std::size_t j = i+1; j < size; ++j )
        diameter2 = std::max( diameter2, (x - corners_[ j ]).two_norm2() );
    }

    return std::sqrt( diameter2 );
  }

private:
  __CoordinateStorage< Geometry, cached > corners_;
};



template< class Geometry, bool cached = false >
typename Geometry::ctype __diameter ( const Geometry &geometry )
{
  __Diameter< Geometry, cached > diameter( geometry );
  return diameter.compute();
}

#endif // #ifndef DOXYGEN



/** \fn diameter
 *
 *  \brief return diameter of entity
 *
 *  \returns diameter
 */
template< int codim, int dim, class Grid, template< int, int, class > class Implementation >
typename Grid::ctype diameter ( const Dune::Entity< codim, dim, Grid, Implementation > &entity )
{
  return __diameter( entity.geometry() );
}

/** \fn diameter
 *
 *  \brief return diameter of intersection
 *
 *  \returns diameter
 */
template< class Grid, class Implementation >
typename Grid::ctype diameter ( const Dune::Intersection< Grid, Implementation > &intersection )
{
  return __diameter( intersection.geometry() );
}

#endif // #ifndef EXAMPLES_POISSON_DIAMETER_HH
