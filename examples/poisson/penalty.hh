#ifndef EXAMPLES_POISSON_PENALTY_HH
#define EXAMPLES_POISSON_PENALTY_HH

#include <algorithm>

#include <dune/fem/io/parameter.hh>

// Penalty
// -------

template< class DiscreteFunctionSpace >
class Penalty
{
public:
  /** \brief field type */
  using FieldType = typename DiscreteFunctionSpace::FunctionSpaceType::RangeFieldType;

  /** \brief grid part type */
  using GridPartType = typename DiscreteFunctionSpace::GridPartType;
  /** \brief intersection type */
  using IntersectionType = typename GridPartType::IntersectionType;

  /** \brief please doc me */
  struct Intersection
  {
    static const std::true_type Interior;
    static const std::false_type Boundary;
  };

  /** \name Construction
   *  \{
   */

  explicit Penalty ( const DiscreteFunctionSpace &space )
    : space_( space ),
      gamma_( Dune::Fem::Parameter::getValue< FieldType >( "ip.solver.penalty" ) )
  {}

  Penalty ( const DiscreteFunctionSpace &space, FieldType gamma )
    : space_( space ),
      gamma_( gamma )
  {}

  Penalty ( const Penalty & ) = delete;

  Penalty &operator= ( const Penalty & ) = delete;

  /** \} */

  /** \name Public member methods
   *  \{
   */

  FieldType gamma ( const IntersectionType &intersection ) const
  {
    return gamma_;
  }

  FieldType sigma ( const IntersectionType &intersection ) const DUNE_DEPRECATED
  {
    if( intersection.neighbor() )
      return sigma( intersection, Intersection::Interior );
    else
      return sigma( intersection, Intersection::Boundary );
  }

  FieldType sigma ( const IntersectionType &intersection, std::true_type ) const
  {
    using EntityType = typename IntersectionType::Entity;
    const EntityType &inside = intersection.inside();
    assert( intersection.neighbor() );
    const EntityType &outside = intersection.outside();

    FieldType h = std::min( inside.geometry().volume(), outside.geometry().volume() );
    h /= intersection.geometry().volume();

    const int p = space().order( inside );
    const int q = space().order( outside );

    return 0.5*gamma_*(p*p + q*q)/h;
  }

  FieldType sigma ( const IntersectionType &intersection, std::false_type ) const
  {
    using EntityType = typename IntersectionType::Entity;
    const EntityType &inside = intersection.inside();

    FieldType h = inside.geometry().volume();
    h /= intersection.geometry().volume();

    const int p = space().order( inside );

    return gamma_*p*p/h;
  }

  /** \} */

private:
  const DiscreteFunctionSpace &space () const
  {
    return space_.get();
  }

  std::reference_wrapper< const DiscreteFunctionSpace > space_;
  FieldType gamma_;
};

#endif // #ifndef EXAMPLES_POISSON_PENALTY_HH
