#ifndef EXAMPLES_POISSON_WAVEFRONT_HH
#define EXAMPLES_POISSON_WAVEFRONT_HH

#include <cmath>

#include <dune/fem/function/common/function.hh>

#include "problemdata.hh"

// ArcTangent
// ----------

struct ArcTangent
{
  static double evaluate ( double x ) noexcept
  {
    return std::atan( x );
  }

  static double jacobian ( double x ) noexcept
  {
    return 1./(1 + x*x);
  }

  static double hessian ( double x ) noexcept
  {
    double y = 1 + x*x;
    return -2*x/(y*y);
  }
};



// WaveFront
// ---------

template< class FunctionSpace >
class WaveFront final
  : public Dune::Fem::Function< FunctionSpace, WaveFront< FunctionSpace > >
{
  using BaseType = Dune::Fem::Function< FunctionSpace, WaveFront< FunctionSpace > >;

public:
  using RangeFieldType = typename BaseType::RangeFieldType;

  using DomainType = typename BaseType::DomainType;
  using RangeType = typename BaseType::RangeType;
  using JacobianRangeType = typename BaseType::JacobianRangeType;

private:
  static_assert( DomainType::dimension == 2, "Wave front problem only implemented for domain dimension 2" );
  static_assert( RangeType::dimension == 1, "Wave front problem only implemented for range dimension 1" );

public:
  WaveFront () noexcept
    : x0_( -0.05 ),
      r0_( 0.7 ),
      alpha_( 20. )
  {}

  void evaluate ( const DomainType &x, RangeType &value ) const noexcept
  {
    const auto y = x - x0_;
    const auto r = y.two_norm();
    const auto p = alpha_*(r - r0_);

    value = ArcTangent::evaluate( p );
  }

  void jacobian ( const DomainType &x, JacobianRangeType &jacobian ) const noexcept
  {
    const auto y = x - x0_;
    const auto r = y.two_norm();
    const auto p = alpha_*(r - r0_);

    jacobian[ 0 ] = y;
    jacobian[ 0 ] *= alpha_*ArcTangent::jacobian( p )/r;
  }

private:
  DomainType x0_;
  RangeFieldType r0_, alpha_;
};



// RightHandSide
// -------------

template< class FunctionSpace >
class RightHandSide< WaveFront< FunctionSpace > > final
  : public Dune::Fem::Function< FunctionSpace, RightHandSide< WaveFront< FunctionSpace > > >
{
  using BaseType = Dune::Fem::Function< FunctionSpace, RightHandSide< WaveFront< FunctionSpace > > >;

public:
  using RangeFieldType = typename BaseType::RangeFieldType;

  using DomainType = typename BaseType::DomainType;
  using RangeType = typename BaseType::RangeType;

  RightHandSide () noexcept
    : x0_( -0.05 ),
      r0_( 0.7 ),
      alpha_( 20. )
  {}

  void evaluate ( const typename BaseType::DomainType &x, typename BaseType::RangeType &value ) const
  {
    const auto y = x - x0_;
    const auto r = y.two_norm();
    const auto p = alpha_*(r - r0_);

    value = alpha_*ArcTangent::jacobian( p )/r;
    value += alpha_*alpha_*ArcTangent::hessian( p );
    value *= -1;
  }

private:
  DomainType x0_;
  RangeFieldType r0_, alpha_;
};

#endif // #ifndef EXAMPLES_POISSON_WAVEFRONT_HH
