set( HEADERS
  diameter.hh
  energynorm.hh
  gridfunction.hh
  indicator.hh
  l2error.hh
  marking.hh
  penalty.hh
  problemdata.hh
  reentrantcorner.hh
  regularity.hh
  solver.hh
  wavefront.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/examples/poisson )

set( DGFFILES
  reentrantcorner.dgf
  wavefront.dgf
)

foreach( dgffile ${DGFFILES} )
  configure_file( ${dgffile} ${CMAKE_CURRENT_BINARY_DIR}/${dgffile} COPYONLY )
endforeach()

foreach( grid alugrid_cube alugrid_simplex )
  foreach( order RANGE 3 8 )
    set( test "poisson_${grid}_${order}" )
    add_executable( ${test} EXCLUDE_FROM_ALL poisson.cc )
    string( TOUPPER ${grid} GRID )
    target_compile_definitions( ${test} PRIVATE "USE_${GRID}" "ORDER=${order}" )
    if( (${GRID} STREQUAL ALUGRID_SIMPLEX) AND (${order} GREATER 6) )
      target_compile_definitions( ${test} PRIVATE "USE_DUNE_QUADRATURES" )
    endif()
    target_link_dune_default_libraries( ${test} )
    add_dune_mpi_flags( ${test} )
  endforeach()
endforeach()
