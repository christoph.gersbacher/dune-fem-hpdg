#ifndef EXAMPLES_POISSON_L2ERROR_HH
#define EXAMPLES_POISSON_L2ERROR_HH

#include <dune/fem/misc/l2norm.hh>

// l2Error
// -------

template< class GridFunction, class DiscreteFunction >
typename GridFunction::RangeFieldType
l2Error ( const GridFunction &u, const DiscreteFunction &uh )
{
  Dune::Fem::L2Norm< typename GridFunction::GridPartType > norm( u.gridPart() );
  return norm.distance( u, uh );
}

#endif // #ifndef EXAMPLES_POISSON_L2ERROR_HH
