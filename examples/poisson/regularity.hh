#ifndef EXAMPLES_POISSON_REGULARITY_HH
#define EXAMPLES_POISSON_REGULARITY_HH

#include <cassert>
#include <cmath>
#include <cstddef>

#include <algorithm>
#include <string>
#include <utility>

#include <dune/common/bartonnackmanifcheck.hh>
#include <dune/common/dynvector.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>

#include <dune/fem/function/grid/default.hh>
#include <dune/fem/function/localfunction/localfunction.hh>
#include <dune/fem/function/localfunction/singlevalued.hh>
#include <dune/fem/hpdg/space/shapefunctionset/orthonormal.hh>
#include <dune/fem/quadrature/elementquadrature.hh>
#include <dune/fem/space/basisfunctionset/default.hh>
#include <dune/fem/space/common/discretefunctionspace.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/discontinuousgalerkin/interpolation.hh>
#include <dune/fem/space/shapefunctionset/vectorial.hh>
#include <dune/fem/utility/localh1norm.hh>

// RegularityIndicator
// -------------------

/** \brief interface for regularity indicators
 *
 *  \tparam  Entity  entity type
 *  \tparam  Implementation  real implementation of this interface class
 *
 *  \ingroup Indicator
 */
template< class DiscreteFunction, class Implementation >
class RegularityIndicator
{
public:
  /** \brief discrete function type */
  using DiscreteFunctionType = DiscreteFunction;
  /** \brief local function type type */
  using LocalFunctionType = typename DiscreteFunctionType::LocalFunctionType;
  /** \brief entity type */
  using EntityType = typename LocalFunctionType::EntityType;

protected:
  RegularityIndicator () = default;

public:
  /** \name Public member methods
   *  \{
   */

  /** \brief compute local indicators */
  template< class Mapper, class Array >
  void operator() ( const DiscreteFunctionType &uh, const Mapper &mapper, Array &array )
  {
    const typename DiscreteFunctionType::GridPartType &gridPart = uh.gridPart();
    auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
    auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
    for( ; it != end; ++it )
    {
      const EntityType &entity = *it;
      array[ mapper.index( entity ) ] = (*this)( uh.localFunction( entity ) );
    }
  }

  /** \brief return estimate for \f$p\f$-refinement
   *
   *  \param[in]  localFunction  a local function
   *
   *  \returns an estimate for the polynomial order (first return value) valid
   *           if the second return value is \b true; otherwise \f$h\f$-refinement
   *           should be used
   */
  std::pair< int, bool > operator() ( const LocalFunctionType &localFunction )
  {
    CHECK_INTERFACE_IMPLEMENTATION( impl()( localFunction ) );
    return impl()( localFunction );
  }

  /** \} */

protected:
  Implementation &impl ()
  {
    return static_cast< Implementation & >( *this );
  }
};



// Prior2P
// -------

/** \brief please doc me
 *
 *  \tparam  DiscreteFunction  a discrete function
 *
 *  <blockquote>
 *    E. Süli, P. Houston, and Ch. Schwab.
 *    \f$hp\f$-finite element methods for hyperbolic problems.
 *    The Mathematics of Finite Elements and Applications X. MAFELAP (J.R. Whiteman, ed.),
 *    Elsevier, 2000, pp. 143--162.
 *  </blockquote>
 *
 *  \ingroup Indicator
 */
template< class DiscreteFunction >
class Prior2P
  : public RegularityIndicator< DiscreteFunction, Prior2P< DiscreteFunction > >
{
  using ThisType = Prior2P< DiscreteFunction >;
  using BaseType = RegularityIndicator< DiscreteFunction, Prior2P< DiscreteFunction > >;

public:
  /** \copydoc RegularityIndicator::DiscreteFunctionType */
  using DiscreteFunctionType = typename BaseType::DiscreteFunctionType;
  /** \copydoc RegularityIndicator::LocalFunctionType */
  using LocalFunctionType = typename BaseType::LocalFunctionType;
  /** \copydoc RegularityIndicator::EntityType */
  using EntityType = typename BaseType::EntityType;

private:
  using RangeType = typename LocalFunctionType::RangeType;
  using RangeFieldType = typename RangeType::value_type;
  static_assert( RangeType::dimension == 1, "" );

  using ScalarShapeFunctionSetType = Dune::Fem::hpDG::OrthonormalShapeFunctionSet< Dune::Fem::FunctionSpace< typename EntityType::Geometry::ctype, RangeFieldType, EntityType::mydimension, 1 > >;
  using ShapeFunctionSetType = Dune::Fem::VectorialShapeFunctionSet< ScalarShapeFunctionSetType, RangeType >;

  using BasisFunctionSetType = Dune::Fem::DefaultBasisFunctionSet< EntityType, ShapeFunctionSetType >;

  using LocalDofVectorType = Dune::DynamicVector< RangeFieldType >;

  using GridPartType = typename DiscreteFunctionType::GridPartType;
  using QuadratureType = Dune::Fem::ElementQuadrature< GridPartType, EntityType::codimension >;
  using InterpolationType = Dune::Fem::DiscontinuousGalerkinLocalL2Projection< GridPartType, BasisFunctionSetType, QuadratureType >;

public:
  /** \name Public member methods
   *  \{
   */

  using BaseType::operator();

  std::pair< int, bool > operator() ( const LocalFunctionType &u )
  {
    const int order = u.order();
    if( order < 3 )
      return std::make_pair( 3, true );

    auto &v = project( u );

    double residual[ 2 ];
    for( std::size_t i = 0u; i < size( order-2 ); ++i )
      v[ i ] = static_cast< RangeFieldType >( 0 );
    residual[ 0 ] = Dune::Fem::LocalH1Norm::norm( v );

    for( std::size_t i = size( order-2 ); i < size( order-1 ); ++i )
      v[ i ] = static_cast< RangeFieldType >( 0 );
    residual[ 1 ] = Dune::Fem::LocalH1Norm::norm( v );

    if( residual[ 0 ] > threshold() && residual[ 1 ] > threshold() )
    {
      const int m = std::ceil( 1 - 0.5*std::log( residual[ 1 ]/ residual[ 0 ] ) / std::log( double( order - 1 ) / double( order - 2 ) ) );
      assert( m == m );

      if( order <= m-1 )
        return std::make_pair( order+1, true );
    }
    else if( residual[ 0 ] + residual[ 1 ] <= 2*threshold() )
      return std::make_pair( std::max( 3, order-1 ), true );

    return std::make_pair( order, false );
  }

  /** \} */

private:
  static RangeFieldType threshold () noexcept { return 1e-8; }

  static constexpr std::size_t size ( int order )
  {
    return RangeType::dimension*ScalarShapeFunctionSetType::size( order );
  }

  template< class LocalFunction >
  Dune::Fem::LocalFunction< BasisFunctionSetType, LocalDofVectorType >
  &project ( const LocalFunction &localFunction ) const
  {
    const auto interpolation
      = this->interpolation( localFunction.entity(), localFunction.order() );

    localFunction_.init( interpolation.basisFunctionSet() );
    interpolation( localFunction, localFunction_.localDofVector() );

    return localFunction_;
  }

  static InterpolationType interpolation ( const EntityType &entity, int p )
  {
    return InterpolationType( basisFunctionSet( entity, p ) );
  }

  static BasisFunctionSetType basisFunctionSet ( const EntityType &entity, int p )
  {
    return BasisFunctionSetType( entity, shapeFunctionSet( entity.type(), p ) );
  }

  static ShapeFunctionSetType shapeFunctionSet ( Dune::GeometryType type, int order )
  {
    return ShapeFunctionSetType( ScalarShapeFunctionSetType( type, order ) );
  }

  mutable Dune::Fem::LocalFunction< BasisFunctionSetType, LocalDofVectorType > localFunction_;
};

#endif // #ifndef EXAMPLES_POISSON_REGULARITY_HH
