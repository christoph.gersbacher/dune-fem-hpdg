#ifndef EXAMPLES_POISSON_PROBLEMDATA_HH
#define EXAMPLES_POISSON_PROBLEMDATA_HH

#include <cassert>

#include <dune/fem/function/common/function.hh>
#include <dune/fem/function/localfunction/localizedfunction.hh>

// RightHandSide
// -------------

template< class Function >
class RightHandSide;



// BoundaryData
// ------------

template< class Function, class Intersection >
class BoundaryData
  : public Dune::Fem::Function< typename Function::FunctionSpaceType, BoundaryData< Function, Intersection > >
{
  using ThisType = BoundaryData< Function, Intersection >;
  using BaseType = Dune::Fem::Function< typename Function::FunctionSpaceType, BoundaryData< Function, Intersection > >;

public:
  /** \brief function type */
  using FunctionType = Function;

  /** \brief entity type */
  using EntityType = Intersection;
  /** \brief local function type */
  using LocalFunctionType = Dune::Fem::LocalizedFunction< FunctionType, EntityType >;

  /** \name Construction
   *  \{
   */

  BoundaryData ( const FunctionType &function, int order )
    : function_( function ),
      order_( order )
  {}

  BoundaryData ( const ThisType & ) = default;

  BoundaryData ( ThisType && ) = default;

  /** \} */

  /** \name Assignment
   *  \{
   */

  ThisType &operator= ( const ThisType & ) = default;

  ThisType &operator= ( ThisType && ) = default;

  /** \} */

  /** \name Public member methods
   *  \{
   */

  bool contains ( const Intersection &intersection ) const
  {
    return intersection.boundary();
  }

  bool contains ( ... ) { return false; }

  LocalFunctionType localFunction ( const EntityType &entity ) const
  {
    assert( contains( entity ) );
    return LocalFunctionType( function_, entity, order_ );
  }

  /** \} */

private:
  FunctionType function_;
  int order_;
};

#endif // #ifndef EXAMPLES_POISSON_PROBLEMDATA_HH
