#ifndef EXAMPLES_POISSON_ENERGYNORM_HH
#define EXAMPLES_POISSON_ENERGYNORM_HH

#include <cassert>
#include <cmath>
#include <cstddef>

#include <algorithm>
#include <functional>
#include <type_traits>

#include <dune/fem/function/common/discretefunction.hh>
#include <dune/fem/function/grid/gridfunction.hh>
#include <dune/fem/function/localfunction/continuous.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>

#include "penalty.hh"

// EnergyNorm
// ----------

/** \brief please doc me
 *
 *  This is an implemenation of the DG energy norm as defined in:
 *  <blockquote>
 *    P. Houston, E. Süli, and T. Wihler. A posteriori error analysis of
 *    \f$hp\f$-version discontinuous Galerkin finite-element methods for
 *    second-order quasi-linear elliptic PDEs. IMA Journal of Numerical
 *    Analysis, 28 (2008), pp. 245--273.
 *  </blockquote>

 *  \tparam  DiscreteFunctionSpace  Discontinuous Galerkin space
 *
 *  \ingroup Indicator
 */
template< class DiscreteFunctionSpace >
class EnergyNorm
{
  using GridPartType = typename DiscreteFunctionSpace::GridPartType;
  using EntityType = typename GridPartType::template Codim< 0 >::EntityType;
  using IntersectionType = typename GridPartType::IntersectionType;

  template< class LF1, class LF2 >
  class LocalFunctionDifference;
  template< class U, class V >
  class GridFunctionDifference;

public:
  /** \brief penalty type */
  using PenaltyType = Penalty< DiscreteFunctionSpace >;

  /** \name Construction
   *  \{
   */

  EnergyNorm () = default;

  explicit EnergyNorm ( const PenaltyType &penalty )
    : penalty_( penalty )
  {}

  /** \} */

  /** \name Public member methods
   *  \{
   */

  template< class U >
  typename U::RangeFieldType operator() ( const U &u ) const
  {
    return norm( u );
  }

  template< class U >
  typename U::RangeFieldType norm ( const U &u ) const
  {
    return std::sqrt( norm2( u ) );
  }

  template< class U >
  typename U::RangeFieldType norm2 ( const U &u ) const;

  template< class U, class V >
  typename U::RangeFieldType distance ( const U &u, const V &v ) const
  {
    return std::sqrt( distance2( u, v ) );
  }

  template< class U, class V >
  typename U::RangeFieldType distance2 ( const U &u, const V &v ) const
  {
    static_assert( std::is_same< typename U::GridPartType, typename V::GridPartType >::value, "" );
    static_assert( std::is_same< typename U::FunctionSpaceType, typename V::FunctionSpaceType >::value, "" );
    return norm2( GridFunctionDifference< U, V >( u, v ) );
  }

  /** \} */

private:
  const PenaltyType &penalty () const
  {
    return penalty_.get();
  }

  template< class LocalFunction >
  typename LocalFunction::RangeFieldType volume2 ( const EntityType &entity, int order,
                                                   const LocalFunction &inside ) const;

  template< bool conforming, class LocalFunction >
  typename LocalFunction::RangeFieldType surface2 ( const IntersectionType &intersection, int order,
                                                    const LocalFunction &inside, const LocalFunction &outside ) const;

  template< class LocalFunction >
  typename LocalFunction::RangeFieldType surface2 ( const IntersectionType &intersection, int order,
                                                    const LocalFunction &inside ) const;
  const GridPartType &gridPart () const
  {
    assert( gridPart_ );
    return *gridPart_;
  }

  std::reference_wrapper< const PenaltyType > penalty_;
  mutable const GridPartType *gridPart_ = nullptr;
};



// EnergyNorm::LocalFunctionDifference
// -----------------------------------

template< class DiscreteFunctionSpace >
template< class LF1, class LF2 >
class EnergyNorm< DiscreteFunctionSpace >::LocalFunctionDifference
  : public Dune::Fem::DefaultContinuousLocalFunction< typename LF1::EntityType, typename LF1::RangeType, LocalFunctionDifference< LF1, LF2 > >
{
  using BaseType = Dune::Fem::DefaultContinuousLocalFunction< typename LF1::EntityType, typename LF1::RangeType, LocalFunctionDifference< LF1, LF2 > >;

public:
  using RangeType = typename BaseType::RangeType;
  using JacobianRangeType = typename BaseType::JacobianRangeType;

  using EntityType = typename BaseType::EntityType;

  /** \name Construction
   *  \{
   */

  LocalFunctionDifference ( const LF1 &lf1, const LF2 &lf2 )
    : localFunctions_( std::make_pair( lf1, lf2 ) )
  {}

  /** \} */

  /** \brief Public member methods
   *  \{
   */

  int order () const
  {
    return std::max( localFunctions_.first.order(), localFunctions_.second.order() );
  }

  template< class Point >
  void evaluate ( const Point &x, RangeType &value ) const
  {
    auto y = Dune::Fem::coordinate( x );
    localFunctions_.first.evaluate( y, value );
    RangeType tmp;
    localFunctions_.second.evaluate( y, tmp );
    value -= tmp;
  }

  template< class Point >
  void jacobian ( const Point &x, JacobianRangeType &jacobian ) const
  {
    auto y = Dune::Fem::coordinate( x );
    localFunctions_.first.jacobian( y, jacobian );
    JacobianRangeType tmp;
    localFunctions_.second.jacobian( y, tmp );
    jacobian -= tmp;
  }

  template< class Point >
  void hessian ( const Point &x, typename BaseType::HessianRangeType &hessian ) const
  {
    DUNE_THROW( Dune::NotImplemented, "Method hessian not implemented yet" );
  }

  const EntityType &entity () const
  {
    return localFunctions_.first.entity();
  }

  /** \} */

private:
  std::pair< LF1, LF2 > localFunctions_;
};



// EnergyNorm::GridFunctionDifference
// ----------------------------------

template< class DiscreteFunctionSpace >
template< class U, class V >
class EnergyNorm< DiscreteFunctionSpace >::GridFunctionDifference
  : public Dune::Fem::GridFunction< GridPartType, LocalFunctionDifference< typename U::LocalFunctionType, typename V::LocalFunctionType >, GridFunctionDifference< U, V > >
{
  using BaseType = Dune::Fem::GridFunction< GridPartType, LocalFunctionDifference< typename U::LocalFunctionType, typename V::LocalFunctionType >, GridFunctionDifference< U, V > >;

public:
  using LocalFunctionType = typename BaseType::LocalFunctionType;

  GridFunctionDifference ( const U &u, const V &v )
    : u_( u ), v_( v )
  {}

  LocalFunctionType localFunction ( const typename BaseType::EntityType &entity ) const
  {
    return LocalFunctionType( u_.localFunction( entity ), v_.localFunction( entity ) );
  }

  const GridPartType &gridPart () const
  {
    return u_.gridPart();
  }

private:
  const U &u_;
  const V &v_;
};



// EnergyNorm::norm2
// -----------------

template< class DiscreteFunctionSpace >
template< class U >
inline typename U::RangeFieldType EnergyNorm< DiscreteFunctionSpace >::norm2 ( const U &u ) const
{
  static_assert( std::is_same< typename U::GridPartType, GridPartType >::value, "" );
  static_assert( std::is_base_of< Dune::Fem::HasLocalFunction, U >::value, "" );

  gridPart_ = &u.gridPart();

  using RangeFieldType = typename U::RangeFieldType;
  RangeFieldType energyNorm2 = 0;

  // traverse grid part elements
  const GridPartType &gridPart = u.gridPart();
  auto it = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
  auto end = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
  for( ; it != end; ++it )
  {
    // get local functions
    const EntityType &entity = *it;
    const auto inside = u.localFunction( entity );
    const int order = inside.order();

    // compute volume integral
    const RangeFieldType volume2
      = this->volume2( entity, std::max( 0, order-1 ), inside );
    energyNorm2 += volume2;

    // compute surface integrals
    auto iit = gridPart.ibegin( entity );
    auto iend = gridPart.iend( entity );
    for( ; iit != iend; ++iit )
    {
      const IntersectionType &intersection = *iit;
      if( intersection.neighbor() )
      {
        const auto outside = u.localFunction( entity );

        const RangeFieldType sigma
          = penalty().sigma( intersection, PenaltyType::Intersection::Interior );
        const RangeFieldType surface2
          = intersection.conforming()
              ? this->template surface2< true >( intersection, std::max( order, outside.order() ), inside, outside )
              : this->template surface2< false >( intersection, std::max( order, outside.order() ), inside, outside );
        energyNorm2 += 0.5*sigma*surface2;
      }
      else if( intersection.boundary() )
      {
        const RangeFieldType sigma
          = penalty().sigma( intersection, PenaltyType::Intersection::Boundary );
        const RangeFieldType surface2
          = this->surface2( intersection, order, inside );
        energyNorm2 += sigma*surface2;
      }
    }
  }

  gridPart_ = nullptr;

  energyNorm2 = gridPart.comm().sum( energyNorm2 );
  return energyNorm2;
}



template< class DiscreteFunctionSpace >
template< class LocalFunction >
inline typename LocalFunction::RangeFieldType EnergyNorm< DiscreteFunctionSpace >
  ::volume2 ( const EntityType &entity, int order, const LocalFunction &inside ) const
{
  // create quadrature
  using QuadratureType = Dune::Fem::CachingQuadrature< GridPartType, 0 >;
  QuadratureType quadrature( entity, std::max( 0, order-1 ) );

  // get geometry
  using GeometryType = typename EntityType::Geometry;
  const GeometryType &geometry = entity.geometry();

  // compute volume integral
  typename LocalFunction::RangeFieldType volume2 = 0;
  typename LocalFunction::JacobianRangeType jacobian;
  const std::size_t nop = quadrature.nop();
  for( std::size_t qp = 0u; qp < nop; ++qp )
  {
    const typename GeometryType::LocalCoordinate &x
      = quadrature.point( qp );
    const typename LocalFunction::RangeFieldType weight
      = quadrature.weight( qp )*geometry.integrationElement( x );

    inside.jacobian( quadrature[ qp ], jacobian );

    volume2 += weight*jacobian.frobenius_norm2();
  }
  return volume2;
}



template< class DiscreteFunctionSpace >
template< bool conforming, class LocalFunction >
inline typename LocalFunction::RangeFieldType EnergyNorm< DiscreteFunctionSpace >
  ::surface2 ( const IntersectionType &intersection, int order, const LocalFunction &inside, const LocalFunction &outside ) const
{
  // create quadratures
  using QuadratureType = Dune::Fem::CachingQuadrature< GridPartType, 1 >;
  Dune::Fem::IntersectionQuadrature< QuadratureType, conforming > quadratures( gridPart(), intersection, order );
  const auto &inner = quadratures.inside();
  const auto &outer = quadratures.outside();

  // get geometry
  using GeometryType = typename IntersectionType::Geometry;
  const GeometryType &geometry = intersection.geometry();

  // compute surface integral
  typename LocalFunction::RangeFieldType surface2 = 0;
  typename LocalFunction::RangeType value1, value2;
  const std::size_t nop = inner.nop();
  for( std::size_t qp = 0u; qp < nop; ++qp )
  {
    const typename GeometryType::LocalCoordinate &x
      = inner.localPoint( qp );
    const double weight
      = inner.weight( qp )*geometry.integrationElement( x );

    inside.evaluate( inner[ qp ], value1 );
    outside.evaluate( outer[ qp ], value2 );

    surface2 += weight*(value1 - value2).two_norm2();
  }
  return surface2;
}



template< class DiscreteFunctionSpace >
template< class LocalFunction >
inline typename LocalFunction::RangeFieldType EnergyNorm< DiscreteFunctionSpace >
  ::surface2 ( const IntersectionType &intersection, int order, const LocalFunction &inside ) const
{
  // create quadratures
  using QuadratureType = Dune::Fem::CachingQuadrature< GridPartType, 1 >;
  QuadratureType quadrature( gridPart(), intersection, order, QuadratureType::INSIDE );

  // get geometry
  using GeometryType = typename IntersectionType::Geometry;
  const GeometryType &geometry = intersection.geometry();

  // compute surface integral
  typename LocalFunction::RangeFieldType surface2 = 0;
  typename LocalFunction::RangeType value;
  const std::size_t nop = quadrature.nop();
  for( std::size_t qp = 0u; qp < nop; ++qp )
  {
    const typename GeometryType::LocalCoordinate &x
      = quadrature.localPoint( qp );
    const double weight
      = quadrature.weight( qp )*geometry.integrationElement( x );

    inside.evaluate( quadrature[ qp ], value );

    surface2 += weight*value.two_norm2();
  }
  return surface2;
}

#endif // #ifndef EXAMPLES_POISSON_ENERGYNORM_HH
