AC_DEFUN([DUNE_FEM_HPDG_CHECKS],[])

AC_DEFUN([DUNE_FEM_HPDG_CHECK_MODULE],[
  AC_MSG_NOTICE([Searching for dune-fem-hpdg...])
  DUNE_CHECK_MODULES([dune-fem-hpdg],[fem/hpdg/space/discontinuousgalerkin/blockmapper.hh])
])
