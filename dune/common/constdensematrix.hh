#ifndef DUNE_COMMON_CONSTDENSEMATRIX_HH
#define DUNE_COMMON_CONSTDENSEMATRIX_HH

#include <dune/common/bartonnackmanifcheck.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/ftraits.hh>
#include <dune/common/matvectraits.hh>

namespace Dune
{

  // ConstDenseMatrix
  // ----------------

  template< class Implementation >
  class ConstDenseMatrix
  {
    using Traits = DenseMatVecTraits< Implementation >;

  public:
    /** \copydoc Dune::DenseMatrix::derived_type */
    using derived_type = typename Traits::derived_type;

    /** \copydoc Dune::DenseMatrix::value_type */
    using value_type = typename Traits::value_type;
    /** \copydoc Dune::DenseMatrix::field_type */
    using field_type = value_type;

    /** \copydoc Dune::DenseMatrix::size_type */
    using size_type = typename Traits::size_type;

  protected:
#ifndef DOXYGEN

    ConstDenseMatrix () = default;

#endif // #ifndef DOXYGEN

  public:
    /** \copydoc Dune::DenseMatrix::rows */
    size_type rows () const { return impl().rows(); }

    /** \copydoc Dune::DenseMatrix::cols */
    size_type cols () const { return impl().cols(); }

    /** \copydoc Dune::DenseMatrix::mtv */
    template< class X, class Y >
    void mv ( const X &x, Y &y ) const
    {
      y = 0;
      umv( x, y );
    }

    /** \copydoc Dune::DenseMatrix::mtv */
    template< class X, class Y >
    void mtv ( const X &x, Y &y ) const
    {
      y = 0;
      umtv( x, y );
    }

    /** \copydoc Dune::DenseMatrix::umv */
    template< class X, class Y >
    void umv ( const X &x, Y &y ) const
    {
      usmv( 1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::umtv */
    template< class X, class Y >
    void umtv ( const X &x, Y &y ) const
    {
      usmtv( 1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::umhv */
    template< class X, class Y >
    void umhv ( const X &x, Y &y ) const
    {
      usmhv( 1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::mmv */
    template< class X, class Y >
    void mmv ( const X &x, Y &y ) const
    {
      usmv( -1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::mmtv */
    template< class X, class Y >
    void mmtv ( const X &x, Y &y ) const
    {
      usmtv( -1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::mmhv */
    template< class X, class Y >
    void mmhv ( const X &x, Y &y ) const
    {
      usmhv( -1, x, y );
    }

    /** \copydoc Dune::DenseMatrix::usmv */
    template< class X, class Y >
    void usmv ( const field_type &alpha, const X &x, Y &y ) const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().usmv( alpha, x, y ) );
      impl().usmv( alpha, x, y );
    }

    /** \copydoc Dune::DenseMatrix::usmtv */
    template< class X, class Y >
    void usmtv ( const field_type &alpha, const X &x, Y &y ) const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().usmtv( alpha, x, y ) );
      impl().usmtv( alpha, x, y );
    }

    /** \copydoc Dune::DenseMatrix::usmhv */
    template< class X, class Y >
    void usmhv ( const field_type &alpha, const X &x, Y &y ) const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().usmhv( alpha, x, y ) );
      impl().usmhv( alpha, x, y );
    }

    /** \copydoc Dune::DenseMatrix::frobenius_norm */
    typename FieldTraits< field_type >::real_type frobenius_norm () const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().frobenius_norm() );
      impl().frobenius_norm();
    }

    /** \copydoc Dune::DenseMatrix::frobenius_norm2 */
    typename FieldTraits< field_type >::real_type frobenius_norm2 () const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().frobenius_norm2() );
      impl().frobenius_norm2();
    }

    /** \copydoc Dune::DenseMatrix::infinity_norm */
    typename FieldTraits< field_type >::real_type infinity_norm () const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().infinity_norm() );
      impl().infinity_norm();
    }

    /** \copydoc Dune::DenseMatrix::infinity_norm_real */
    typename FieldTraits< field_type >::real_type infinity_norm_real () const
    {
      CHECK_INTERFACE_IMPLEMENTATION( impl().infinity_norm_real() );
      impl().infinity_norm_real();
    }

  protected:
    const Implementation &impl () const
    {
      return static_cast< const Implementation & >( *this );
    }
  };

} // namespace Dune

#endif // #ifndef DUNE_COMMON_CONSTDENSEMATRIX_HH
