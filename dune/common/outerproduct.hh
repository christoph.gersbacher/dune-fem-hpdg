#ifndef EXAMPLES_POISSON_OUTERPRODUCT_HH
#define EXAMPLES_POISSON_OUTERPRODUCT_HH

#include <cassert>
#include <cstddef>

#include <type_traits>
#include <utility>

#include <dune/common/constdensematrix.hh>
#include <dune/common/densematrix.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/ftraits.hh>
#include <dune/common/matvectraits.hh>

namespace Dune
{

  // Internal forward declaration
  // ----------------------------

  template< class Range, class Domain >
  class OuterProduct;



#ifndef DOXYGEN

  // DenseMatVecTraits
  // -----------------

  template< class Range, class Domain >
  class DenseMatVecTraits< OuterProduct< Range, Domain > >
  {
  public:
    using derived_type = OuterProduct< Range, Domain >;
    using value_type = typename std::common_type< typename Domain::field_type, typename Range::field_type >::type;
    using size_type = std::size_t;
  };



  // FieldTraits
  // -----------

  template< class Range, class Domain >
  struct FieldTraits< OuterProduct< Range, Domain > >
  {
    using field_type = typename DenseMatVecTraits< OuterProduct< Range, Domain > >::value_type;
    using real_type = typename FieldTraits< field_type >::real_type;
  };

#endif // #ifndef DOXYGEN



  // OuterProduct
  // ------------

  template< class Range, class Domain >
  class OuterProduct
    : public ConstDenseMatrix< OuterProduct< Range, Domain > >
  {
    using Base = ConstDenseMatrix< OuterProduct< Range, Domain > >;

  public:
    /** \copydoc Dune::ConstDenseMatrix::field_type */
    using field_type = typename Base::field_type;
    /** \copydoc Dune::ConstDenseMatrix::size_type */
    using size_type = typename Base::size_type;

    /** \name Construction
     *  \{
     */

    OuterProduct ( const Range &u, const Domain &v )
      : u_( u ),
        v_( v )
    {}

    OuterProduct ( Range &&u, Domain &&v )
      : u_( std::forward< Range >( u ) ),
        v_( std::forward< Domain >( v ) )
    {}

    OuterProduct ( const OuterProduct & ) = default;

    OuterProduct ( OuterProduct && ) = default;

    /** \} */

    /** \name Assignment
     *  \{
     */

    OuterProduct &operator= ( const OuterProduct & ) = default;

    OuterProduct &operator= ( OuterProduct && ) = default;

    /** \} */

    /** \brief return number of rows */
    size_type rows () const { return u_.size(); }

    /** \brief return number of columns */
    size_type cols () const { return v_.size(); }

    /** \copydoc Dune::DenseMatrix::mtv */
    template< class X, class Y >
    void mv ( const X &x, Y &y ) const
    {
      y = u_;
      y *= (v_*x);
    }

    /** \copydoc Dune::DenseMatrix::mtv */
    template< class X, class Y >
    void mtv ( const X &x, Y &y ) const
    {
      y = v_;
      y *= (u_*x);
    }

    /** \copydoc Dune::DenseMatrix::umv */
    template< class X, class Y >
    void umv ( const X &x, Y &y ) const
    {
      y.axpy( v_*x, u_ );
    }

    /** \copydoc Dune::DenseMatrix::umtv */
    template< class X, class Y >
    void umtv ( const X &x, Y &y ) const
    {
      y.axpy( u_*x, v_ );
    }

    /** \copydoc Dune::DenseMatrix::umhv */
    template< class X, class Y >
    void umhv ( const X &x, Y &y ) const
    {
      DUNE_THROW( NotImplemented, "Method umhv() not implemented yet" );
    }

    /** \copydoc Dune::DenseMatrix::mmv */
    template< class X, class Y >
    void mmv ( const X &x, Y &y ) const
    {
      y.axpy( -v_*x, u_ );
    }

    /** \copydoc Dune::DenseMatrix::mmtv */
    template< class X, class Y >
    void mmtv ( const X &x, Y &y ) const
    {
      y.axpy( -u_*x, v_ );
    }

    /** \copydoc Dune::DenseMatrix::mmhv */
    template< class X, class Y >
    void mmhv ( const X &x, Y &y ) const
    {
      DUNE_THROW( NotImplemented, "Method mmhv() not implemented yet" );
    }

    /** \copydoc Dune::DenseMatrix::usmv */
    template< class X, class Y >
    void usmv ( const field_type &alpha, const X &x, Y &y ) const
    {
      y.axpy( alpha*(v_*x), u_ );
    }

    /** \copydoc Dune::DenseMatrix::usmtv */
    template< class X, class Y >
    void usmtv ( const field_type &alpha, const X &x, Y &y ) const
    {
      y.axpy( alpha*(u_*x), v_ );
    }

    /** \copydoc Dune::DenseMatrix::usmhv */
    template< class X, class Y >
    void usmhv ( const field_type &alpha, const X &x, Y &y ) const
    {
      DUNE_THROW( NotImplemented, "Method usmhv() not implemente yet" );
    }

    /** \copydoc Dune::DenseMatrix::frobenius_norm */
    typename FieldTraits< field_type >::real_type frobenius_norm () const
    {
      DUNE_THROW( NotImplemented, "Method frobenius_norm() not implemented yet" );
    }

    /** \copydoc Dune::DenseMatrix::frobenius_norm2 */
    typename FieldTraits< field_type >::real_type frobenius_norm2 () const
    {
      DUNE_THROW( NotImplemented, "Method frobenius_norm2() not implemented yet" );
    }

    /** \copydoc Dune::DenseMatrix::infinity_norm */
    typename FieldTraits< field_type >::real_type infinity_norm () const
    {
      DUNE_THROW( NotImplemented, "Method infinity_norm() not implemented yet" );
    }

    /** \copydoc Dune::DenseMatrix::infinity_norm_real */
    typename FieldTraits< field_type >::real_type infinity_norm_real () const
    {
      DUNE_THROW( NotImplemented, "Method infinity_norm_real() not implemented yet" );
    }

  private:
    template< class, class > friend class DenseMatrixAssigner;

    Range u_;
    Domain v_;
  };



  // outerProduct
  // ------------

  template< class Range, class Domain >
  OuterProduct< Range, Domain > outerProduct ( const Range &u, const Domain &v )
  {
    return OuterProduct< Range, Domain >( u, v );
  }



  // Template specialization of DenseMatrixAssigner
  // ----------------------------------------------

  template< class DenseMatrix, class Range, class Domain >
  class DenseMatrixAssigner< DenseMatrix, OuterProduct< Range, Domain > >
  {
  public:
    static void apply ( DenseMatrix &denseMatrix, const OuterProduct< Range, Domain > &outerProduct )
    {
      const std::size_t rows = outerProduct.rows();
      assert( denseMatrix.N() == rows );

      const std::size_t cols = outerProduct.cols();
      assert( denseMatrix.M() == cols );

      const auto &u = outerProduct.u_;
      const auto &v = outerProduct.v_;
      for( std::size_t i = 0; i < rows; ++i )
        for( std::size_t j = 0; j < cols; ++j )
          denseMatrix[ i ][ j ] = u[ i ]*v[ j ];
    }
  };

} // namespace Dune

#endif // #ifndef EXAMPLES_POISSON_OUTERPRODUCT_HH
