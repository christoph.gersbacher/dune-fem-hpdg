#ifndef DUNE_FEM_TEST_REFINEMENT_HH
#define DUNE_FEM_TEST_REFINEMENT_HH

#include <algorithm>
#include <chrono>
#include <random>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/partitionset.hh>
#include <dune/grid/common/rangegenerators.hh>

// Refinement
// ----------

struct Refinement
{
  Refinement ()
    : engine_( std::chrono::system_clock::now().time_since_epoch().count() )
  {}

  explicit Refinement ( unsigned int seed )
    : engine_( seed )
  {}

  template< class DiscreteFunctionSpace >
  void h_refinement ( DiscreteFunctionSpace &space );

  template< class DiscreteFunctionSpace >
  void p_refinement ( DiscreteFunctionSpace &space );

private:
  std::default_random_engine engine_;
};



// Refinement::h_refinement
// ------------------------

template< class DiscreteFunctionSpace >
void Refinement::h_refinement ( DiscreteFunctionSpace &space )
{
  using GridPartType = typename DiscreteFunctionSpace::GridPartType;
  GridPartType &gridPart = space.gridPart();

  std::uniform_int_distribution< int > distribution( -(1 << GridPartType::dimension), 1 );

  auto first = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
  auto last = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
  for( ; first != last; ++first )
  {
    const auto &entity = *first;
    auto mark = distribution( engine_ ) < 0 ? -1 : 1;
    if( entity.level() == 0 )
      mark = std::max( mark, 0 );
    gridPart.grid().mark( mark, entity );
  }
}



// Refinement::p_refinement
// ------------------------

template< class DiscreteFunctionSpace >
void Refinement::p_refinement ( DiscreteFunctionSpace &space )
{
  using GridPartType = typename DiscreteFunctionSpace::GridPartType;
  GridPartType &gridPart = space.gridPart();

  std::uniform_int_distribution< int > distribution( 0, space.order() );

  auto first = gridPart.template begin< 0, Dune::InteriorBorder_Partition >();
  auto last = gridPart.template end< 0, Dune::InteriorBorder_Partition >();
  for( ; first != last; ++first )
    space.mark( distribution( engine_ ), *first );
}

#endif // #ifndef DUNE_FEM_TEST_REFINEMENT_HH
