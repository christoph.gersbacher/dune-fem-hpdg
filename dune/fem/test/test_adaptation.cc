#include <config.h>

#define WANT_CACHED_COMM_MANAGER 0

#include <cstdlib>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>

#include <dune/common/exceptions.hh>

#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>

#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/interpolate.hh>
#include <dune/fem/space/common/restrictprolongtuple.hh>
#include <dune/fem/test/exactsolution.hh>
#include <dune/fem/function/grid/simple.hh>

#include <dune/fem/hpdg/space/common/adaptationmanager.hh>
#include <dune/fem/hpdg/space/common/dataprojection.hh>
#include <dune/fem/hpdg/space/discontinuousgalerkin/orthogonal.hh>

#include "refinement.hh"

int main ( int argc, char **argv )
try
{
  // initialize MPI
  Dune::Fem::MPIManager::initialize( argc, argv );

  if( argc < 3 )
  {
    std::cerr << "Usage" << argv[ 0 ] << " <dgf file> <steps> <initial> <seed>" << std::endl;
    return 0;
  }

  // read parameters
  Dune::Fem::Parameter::append( argc, argv );
  Dune::Fem::Parameter::appendDGF( argv[ 1 ] );

  // create grid
  using GridType
#if defined USE_ALUGRID_CUBE
    = Dune::ALUGrid< DIM, DIM, Dune::cube, Dune::nonconforming >;
#elif defined USE_ALUGRID_SIMPLEX
    = Dune::ALUGrid< DIM, DIM, Dune::simplex, Dune::nonconforming >;
#endif
  Dune::GridPtr< GridType > grid( argv[ 1 ] );
  grid->loadBalance();

  // initial refinement
  if( argc > 3 )
  {
    const int refineStepsForHalf = Dune::DGFGridInfo< GridType >::refineStepsForHalf();
    const int count = std::max( 0, std::atoi( argv[ 3 ] ) );
    Dune::Fem::GlobalRefine::apply( *grid, count*refineStepsForHalf );
  }

  // create grid part
  using GridPartType = Dune::Fem::AdaptiveLeafGridPart< GridType >;
  GridPartType gridPart( *grid );

  // create grid function
  using FunctionSpaceType = Dune::Fem::FunctionSpace< GridPartType::ctype, double, GridPartType::dimensionworld, 3 >;
  Dune::Fem::ExactSolution< FunctionSpaceType > function;
  Dune::Fem::GridFunctionAdapter< decltype( function ), GridPartType > gridFunction( "grid function", function, gridPart, 1 );

  // create discrete function space
  using DiscreteFunctionSpaceType = Dune::Fem::hpDG::OrthogonalDiscontinuousGalerkinSpace< FunctionSpaceType, GridPartType, 2 >;
  DiscreteFunctionSpaceType space( gridPart );

  // create discrete function
  using DiscreteFunctionType = Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType >;
  DiscreteFunctionType uh( "uh", space );
  Dune::Fem::interpolate( gridFunction, uh );

  // file I/O
  using ElementType = DiscreteFunctionSpaceType::EntityType;
  using LocalCoordinateType = ElementType::Geometry::LocalCoordinate;
  auto ph = Dune::Fem::simpleGridFunction( "p", gridPart, [&space]( const ElementType &element, const LocalCoordinateType &) -> Dune::FieldVector< double, 1 > { return space.order( element ); }, 0 );
  auto tuple = std::make_tuple( &uh, &ph );
  Dune::Fem::DataOutput< GridType, decltype( tuple ) > output( *grid, tuple );
  output.write();

  // create adaptation manager for grid refinement
  using RestrictionProlongationType = Dune::Fem::RestrictProlongDefaultTuple< DiscreteFunctionType >;
  RestrictionProlongationType restrictProlong( uh );
  restrictProlong.setFatherChildWeight( Dune::DGFGridInfo< GridType >::refineWeight() );
  using GridAdaptationManagerType = Dune::Fem::AdaptationManager< GridType, RestrictionProlongationType >;
  GridAdaptationManagerType gridAdaptManager( *grid, restrictProlong );

  // create adaptation manager for p-refinement
  using DataProjectionType = Dune::Fem::hpDG::DefaultDataProjectionTuple< DiscreteFunctionType >;
  DataProjectionType dataProjection( uh );
  using SpaceAdaptationManagerType = Dune::Fem::hpDG::AdaptationManager< DiscreteFunctionSpaceType, DataProjectionType >;
  SpaceAdaptationManagerType spaceAdaptManager( space, std::move( dataProjection ) );

  // seed
  unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  if( argc > 4 )
    seed = static_cast< unsigned int >( std::atoi( argv[ 4 ] ) );
  std::cerr << "Using seed " << seed << std::endl;

  // adapt grid
  Refinement refinement( seed );
  const int steps = std::atoi( argv[ 2 ] );
  for( int step = 0; step < steps; ++step )
  {
    if( step % 2 )
    {
      refinement.h_refinement( space );
      gridAdaptManager.adapt();
    }
    else
    {
      refinement.p_refinement( space );
      spaceAdaptManager.adapt();
    }
    output.write();
  }

  return 0;
}
catch( Dune::Exception &exception )
{
  std::cerr << exception << std::endl;
  return 1;
}
catch( std::exception &exception )
{
  std::cerr << exception.what() << std::endl;
  return 1;
}
catch( ... )
{
  std::cerr << "Uknown exception thrown" << std::endl;
  return 1;
}
