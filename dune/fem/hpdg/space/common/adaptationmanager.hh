#ifndef DUNE_FEM_HPDG_SPACE_COMMON_ADAPTATIONMANAGER_HH
#define DUNE_FEM_HPDG_SPACE_COMMON_ADAPTATIONMANAGER_HH

#include <cassert>
#include <cstddef>

#include <functional>
#include <utility>
#include <vector>

#include <dune/common/timer.hh>

#include <dune/fem/misc/threads/threadmanager.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/space/common/communicationmanager.hh>
#include <dune/fem/space/common/dofmanager.hh>

#include "dataprojection.hh"

namespace Dune
{

  namespace Fem
  {

    namespace hpDG
    {

      // AdaptationManager
      // -----------------

      /** \brief Manages the testriction and prolongation of discrete functions in \f$(h)p\f$-adaptive computations
       *
       *  \tparam  DiscreteFunctionSpace  an adaptive discrete function space
       *  \tparam  DataProjection  a DataProjection type
       *
       *  \ingroup DiscreteFunctionSpace_RestrictProlong
       */
      template< class DiscreteFunctionSpace, class DataProjection >
      class AdaptationManager final
      : public Dune::Fem::AdaptationManagerInterface
      {
        using ThisType = AdaptationManager< DiscreteFunctionSpace, DataProjection >;

      public:
        /** \brief discrete function space type */
        using DiscreteFunctionSpaceType = DiscreteFunctionSpace;
        /** \brief data projection type */
        using DataProjectionType = DataProjection;

      private:
        using GridType = typename DiscreteFunctionSpaceType::GridType;
        using DofManagerType = DofManager< GridType >;

        struct DataProjectionWrapper;

      public:
        /** \name Construction
         *  \{
         */

        explicit AdaptationManager ( DiscreteFunctionSpaceType &space, DataProjectionType &&dataProjection )
          : space_( space ),
            dataProjection_( std::forward< DataProjectionType >( dataProjection ) ),
            dofManager_( DofManagerType::instance( space.gridPart().grid() ) ),
            commList_( dataProjection_ ),
            time_( 0. )
        {}

        /** \} */

        /** \brief Deleted methods
         *  \{
         */

        /** \brief copy constructor */
        AdaptationManager ( const ThisType & ) = delete;

        /** \brief assignment operator */
        ThisType &operator= ( const ThisType & ) = delete;

        /** \} */

        /** \name Adaptation
         *  \{
         */

        /** \brief returns \b true */
        bool adaptive () const { return true; }

        /** \brief perform adaptation */
        void adapt ()
        {
          assert( Dune::Fem::ThreadManager::singleThreadMode() );

          Dune::Timer timer;

          // dofManager().reserveMemory( 0 );

          DataProjectionWrapper wrapper( dataProjection_, dofManager() );
          space().adapt( wrapper );

          if( dofManager().notifyGlobalChange( static_cast< bool >( wrapper ) ) )
            dofManager().compress();

          commList_.exchange();

          time_ = timer.elapsed();
        }

        /** \brief return name of adaptation method */
        const char *methodName () const { return "discrete function space adaptation"; }

        /** \brief return time spent on adaptation */
        double adaptationTime () const { return time_; }

        /** \} */

        /** \name Load balancing
         *  \{
         */

        /** \brief please doc me */
        bool loadBalance () { return false; }

        /** \brief please doc me */
        int balanceCounter () const { return 0; }

        /** \brief please doc me */
        double loadBalanceTime () const { return 0.; }

        /** \} */

      private:
        DiscreteFunctionSpaceType &space () { return space_.get(); }

        const DiscreteFunctionSpaceType &space () const { return space_.get(); }

        DofManagerType &dofManager () { return dofManager_.get(); }

        const DofManagerType &dofManager () const { return dofManager_.get(); }

        std::reference_wrapper< DiscreteFunctionSpaceType > space_;
        DataProjectionType dataProjection_;
        std::reference_wrapper< DofManagerType > dofManager_;
        mutable CommunicationManagerList commList_;
        double time_;
      };



      // AdaptationManager::DataProjectionWrapper
      // ----------------------------------------

      template< class DiscreteFunctionSpace, class DataProjection >
      class AdaptationManager< DiscreteFunctionSpace, DataProjection >::DataProjectionWrapper
      : public Dune::Fem::hpDG::DataProjection< DiscreteFunctionSpace, DataProjectionWrapper >
      {
        using BaseType = Dune::Fem::hpDG::DataProjection< DiscreteFunctionSpace, DataProjectionWrapper >;

      public:
        using BasisFunctionSetType = typename BaseType::BasisFunctionSetType;
        using EntityType = typename BaseType::EntityType;

        DataProjectionWrapper ( DataProjectionType &dataProjection, DofManagerType &dofManager )
          : dataProjection_( dataProjection ),
            dofManager_( dofManager ),
            modified_( false )
        {}

        void operator() ( const EntityType &entity,
                          const BasisFunctionSetType &prior,
                          const BasisFunctionSetType &present,
                          const std::vector< std::size_t > &origin,
                          const std::vector< std::size_t > &destination )
        {
          dofManager_.get().resizeMemory();
          dataProjection_.get()( entity, prior, present, origin, destination );
          modified_ = true;
        }

        explicit operator bool () const
        {
          return modified_;
        }

      private:
        std::reference_wrapper< DataProjectionType > dataProjection_;
        std::reference_wrapper< DofManagerType > dofManager_;
        bool modified_;
      };

    } // namespace hpDG

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_HPDG_SPACE_COMMON_ADAPTATIONMANAGER_HH
