#ifndef DUNE_FEM_HPDG_SPACE_DISCONTINUOUSGALERKIN_INTERPOLATION_HH
#define DUNE_FEM_HPDG_SPACE_DISCONTINUOUSGALERKIN_INTERPOLATION_HH

#include <type_traits>
#include <utility>

#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/projection/local/l2projection.hh>
#include <dune/fem/operator/projection/local/riesz.hh>

namespace Dune
{

  namespace Fem
  {

    namespace hpDG
    {

      // Internal forward declaration
      // ----------------------------

      template< class BasisFunctionSets,
                class Quadrature = CachingQuadrature< typename BasisFunctionSets::GridPartType, BasisFunctionSets::BasisFunctionSetType::EntityType::codimension > >
      class DiscontinuousGalerkinLocalInterpolation;



      // DiscontinuousGalerkinLocalInterpolation
      // ---------------------------------------

      template< class BasisFunctionSets, class Quadrature >
      class DiscontinuousGalerkinLocalInterpolation
      : public Dune::Fem::LocalL2Projection< typename BasisFunctionSets::BasisFunctionSetType, DiscontinuousGalerkinLocalInterpolation< BasisFunctionSets, Quadrature > >
      {
        using ThisType = DiscontinuousGalerkinLocalInterpolation< BasisFunctionSets, Quadrature >;
        using BaseType = Dune::Fem::LocalL2Projection< typename BasisFunctionSets::BasisFunctionSetType, DiscontinuousGalerkinLocalInterpolation< BasisFunctionSets, Quadrature > >;

      public:
        /** \brief basis function set type */
        using BasisFunctionSetType = typename BaseType::BasisFunctionSetType;

      private:
        using GridPartType = typename BasisFunctionSets::GridPartType;
        using QuadratureType = Quadrature;

        using LocalRieszProjectionType = typename std::conditional< BasisFunctionSets::orthogonal(),
            Dune::Fem::OrthonormalLocalRieszProjection< BasisFunctionSetType >,
            Dune::Fem::DenseLocalRieszProjection< BasisFunctionSetType, Quadrature >
          >::type;

        using ImplementationType = Dune::Fem::DefaultLocalL2Projection< LocalRieszProjectionType, Quadrature >;

      public:
        /** \name Construction
         *  \{
         */

        explicit DiscontinuousGalerkinLocalInterpolation ( const BasisFunctionSetType &basisFunctionSet )
          : impl_( LocalRieszProjectionType( basisFunctionSet ) )
        {}

        explicit DiscontinuousGalerkinLocalInterpolation ( BasisFunctionSetType &&basisFunctionSet )
          : impl_( LocalRieszProjectionType( std::forward< BasisFunctionSetType >( basisFunctionSet ) ) )
        {}

        /** \} */

        /** \name Copying and assignment
         *  \{
         */

        /** \brief copy constructor */
        DiscontinuousGalerkinLocalInterpolation ( const ThisType & ) = default;

        /** \brief move constructor */
        DiscontinuousGalerkinLocalInterpolation ( ThisType && ) = default;

        /** \brief assignment operator */
        DiscontinuousGalerkinLocalInterpolation &operator= ( const ThisType & ) = default;

        /** \brief move assignment operator */
        DiscontinuousGalerkinLocalInterpolation &operator= ( ThisType && ) = default;

        /** \} */

        /** \name Public member methods
         *  \{
         */

        /** \brief return basis function set */
        BasisFunctionSetType basisFunctionSet () const
        {
          return impl_.basisFunctionSet();
        }

        /** \brief apply projection */
        template< class LocalFunction, class LocalDofVector >
        void apply ( const LocalFunction &localFunction, LocalDofVector &localDofVector ) const
        {
          impl_( localFunction, localDofVector );
        }

        /** \} */

      private:
        ImplementationType impl_;
      };

    } // namespace hpDG

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_HPDG_SPACE_DISCONTINUOUSGALERKIN_INTERPOLATION_HH
