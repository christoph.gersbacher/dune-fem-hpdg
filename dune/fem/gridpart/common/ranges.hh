#ifndef DUNE_FEM_GRIDPART_COMMON_RANGES_HH
#define DUNE_FEM_GRIDPART_COMMON_RANGES_HH

#include <type_traits>
#include <utility>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/fem/gridpart/common/gridpart.hh>

namespace Dune
{

  namespace Fem
  {

    // isGridPart
    // ----------

#ifndef DOXYGEN

    template< class Traits >
    std::true_type __isGridPart ( const Dune::Fem::GridPartInterface< Traits > & );
    template< class... >
    std::false_type __isGridPart ( ... );

#endif // #ifndef DOXYGEN

    template< class GridPart >
    using isGridPart = decltype( __isGridPart( std::declval< GridPart >() ) );



    // GridPartEntityIteratorTraits
    // ----------------------------

    template< typename GridPart, int codim, unsigned int partitions,
              bool enable = isGridPart< GridPart >::value >
    class GridPartEntityIteratorTraits;

    template< typename GridPart, int codim, unsigned int partitions >
    class GridPartEntityIteratorTraits< GridPart, codim, partitions, true >
    {
      static_assert( 0 <= codim && codim <= GridPart::dimension, "Invalid codimension for given GridPart" );

    public:
      /** \brief partition iterator type */
      static const PartitionIteratorType pitype
        = derive_partition_iterator_type< partitions >::value;
      /** \brief iterator type */
      using IteratorType
        = typename GridPart::template Codim< codim >::template Partition< pitype >::IteratorType;

      /** \brief return begin iterator */
      static IteratorType begin ( const GridPart &gridPart )
      {
        return gridPart.template begin< codim, pitype >();
      }

      /** \brief return begin iterator */
      static IteratorType end ( const GridPart &gridPart )
      {
        return gridPart.template end< codim, pitype >();
      }
    };

  } // namespace Fem



  // entities
  // --------

  template< class GridPart, int codim, unsigned int partitions >
  inline typename std::enable_if< Fem::isGridPart< GridPart >::value, IteratorRange< typename Fem::GridPartEntityIteratorTraits< GridPart, codim, partitions >::IteratorType > >::type
  entities ( const GridPart &gridPart, Codim< codim >, PartitionSet< partitions > )
  {
    using Traits = Fem::GridPartEntityIteratorTraits< GridPart, codim, partitions >;
    using IteratorRangeType = IteratorRange< typename Traits::IteratorType >;
    return IteratorRangeType( Traits::begin( gridPart ), Traits::end( gridPart ) );
  }

  template< class GridPart, class Entity >
  inline typename std::enable_if< Fem::isGridPart< GridPart >::value, IteratorRange< typename GridPart::IntersectionIteratorType > >::type
  intersections ( const GridPart &gridPart, const Entity &entity )
  {
    using IteratorRangeType = IteratorRange< typename GridPart::IntersectionIteratorType >;
    return IteratorRangeType( gridPart.ibegin( entity ), gridPart.iend( entity ) );
  }

} // namespace Dune

#endif // #ifndef DUNE_FEM_GRIDPART_COMMON_RANGES_HH
