#ifndef DUNE_FEM_UTILITY_LOCALH1NORM_HH
#define DUNE_FEM_UTILITY_LOCALH1NORM_HH

#include <cmath>
#include <cstddef>

#include <dune/fem/common/coordinate.hh>

#include <dune/fem/function/localfunction/difference.hh>
#include <dune/fem/quadrature/geometric/gausslegendre.hh>

namespace Dune
{

  namespace Fem
  {

    // LocalH1Norm
    // -----------

    class LocalH1Norm
    {
    public:
      /** \brief compute local H1 norm */
      template< class LocalFunction >
      static typename LocalFunction::RangeFieldType norm ( const LocalFunction &u )
      {
        return std::sqrt( norm2( u ) );
      }

      /** \brief compute square of local H1 norm */
      template< class LocalFunction, class LocalDiscreteFunction >
      static typename LocalFunction::RangeFieldType
      norm ( const LocalFunction &u, const LocalDiscreteFunction &uh )
      {
        return std::sqrt( distance2( u, uh ) );
      }

      /** \brief compute local H1 error */
      template< class LocalFunction >
      static typename LocalFunction::RangeFieldType norm2 ( const LocalFunction &u )
      {
        using GeometryType = typename LocalFunction::EntityType::Geometry;
        const GeometryType geometry = u.entity().geometry();

        using QuadratureType = Dune::Fem::GaussLegendreQuadrature< typename GeometryType::ctype, GeometryType::mydimension >;
        QuadratureType quadrature( geometry.type(), 2*u.order() );

        using RangeFieldType = typename LocalFunction::RangeFieldType;
        RangeFieldType norm2( 0 );

        typename LocalFunction::RangeType value;
        typename LocalFunction::JacobianRangeType jacobian;

        const std::size_t nop = quadrature.nop();
        for( std::size_t qp = 0u; qp < nop; ++qp )
        {
          const auto x = quadrature.point( qp );
          const auto weight = geometry.integrationElement( x )*quadrature.weight( qp );

          RangeFieldType phi( 0 );

          u.evaluate( quadrature[ qp ], value );
          phi = value.two_norm2();

          u.jacobian( quadrature[ qp ], jacobian );
          for( int i = 0; i < LocalFunction::dimRange; ++i )
            phi += jacobian[ i ].two_norm2();

          norm2 += weight*phi;
        }
        return norm2;
      }

      /** \brief compute square of local H1 error */
      template< class LocalFunction, class LocalDiscreteFunction >
      static typename LocalFunction::RangeFieldType
      distance2 ( const LocalFunction &u, const LocalDiscreteFunction &uh )
      {
        auto v = LocalFunctionDifference< LocalFunction, LocalDiscreteFunction >( u, uh );
        return norm2( v );
      }
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_UTILITY_LOCALH1NORM_HH
