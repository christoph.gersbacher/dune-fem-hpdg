#ifndef DUNE_FEM_UTILITY_LOCALL2NORM_HH
#define DUNE_FEM_UTILITY_LOCALL2NORM_HH

#include <cmath>
#include <cstddef>

#include <dune/fem/common/coordinate.hh>

#include <dune/fem/function/localfunction/difference.hh>
#include <dune/fem/quadrature/geometric/gausslegendre.hh>

namespace Dune
{

  namespace Fem
  {

    // LocalL2Norm
    // -----------

    class LocalL2Norm
    {
      template< class LocalFunction, class LocalDiscreteFunction >
      class Difference;

    public:
      /** \brief compute local L2 norm */
      template< class LocalFunction >
      static typename LocalFunction::RangeFieldType norm ( const LocalFunction &u )
      {
        return std::sqrt( norm2( u ) );
      }

      /** \brief compute square of local L2 norm */
      template< class LocalFunction, class LocalDiscreteFunction >
      static typename LocalFunction::RangeFieldType
      distance ( const LocalFunction &u, const LocalDiscreteFunction &uh )
      {
        return std::sqrt( distance2( u, uh ) );
      }

      /** \brief compute local L2 error */
      template< class LocalFunction >
      static typename LocalFunction::RangeFieldType norm2 ( const LocalFunction &u )
      {
        using GeometryType = typename LocalFunction::EntityType::Geometry;
        const GeometryType geometry = u.entity().geometry();

        using QuadratureType = Dune::Fem::GaussLegendreQuadrature< typename GeometryType::ctype, GeometryType::mydimension >;
        QuadratureType quadrature( geometry.type(), 2*u.order() );

        using RangeType = typename LocalFunction::RangeType;
        typename RangeType::value_type norm2( 0 );

        const std::size_t nop = quadrature.nop();
        for( std::size_t qp = 0u; qp < nop; ++qp )
        {
          const auto x = quadrature.point( qp );
          const auto weight = geometry.integrationElement( x )*quadrature.weight( qp );

          RangeType value;
          u.evaluate( quadrature[ qp ], value );
          norm2 += weight*value.two_norm2();
        }
        return norm2;
      }

      /** \brief compute square of local L2 error */
      template< class LocalFunction, class LocalDiscreteFunction >
      static typename LocalFunction::RangeFieldType
      distance2 ( const LocalFunction &u, const LocalDiscreteFunction &uh )
      {
        auto v = LocalFunctionDifference< LocalFunction, LocalDiscreteFunction >( u, uh );
        return norm2( v );
      }
    };

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_FEM_UTILITY_LOCALL2NORM_HH
