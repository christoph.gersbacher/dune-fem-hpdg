# The dune-fem-hpdg module

The [dune-fem-hpdg][0] module is an extension module to the [Dune-Fem][1] finite
element library. It provides a generic implementation of hp-adaptive
discontinuous Galerkin spaces, and facilities for handling the restriction and
prolongation of user data in adaptive computations.

## Features

  *  Generic implementation of hp-adaptive discontinuous finite element spaces
  *  Extensible facilities for automated restriction and prolongation of user data
  *  Full support of legacy code and local mesh adaptation facilities from [Dune-Fem][1]

## Citing

A detailed description of the abstraction principles behind the module and its
implementation can be found in the preprint
> Ch. Gersbacher: [*Implementation of hp-adaptive discontinuous finite element methods in Dune-Fem*][2], arXiv: 1604.07242, 2016.

## Installation

For installation instructions please see the [Dune website][3] and the
[installation notes][4] there.

## License

The dune-fem-hpdg module is free open-source software, licensed under version 2
or later of the GNU General Public License.  See the file [LICENSE.md][5] for
full copying permissions.

[0]: https://gitlab.dune-project.org/christoph.gersbacher/dune-fem-hpdg.git
[1]: https://gitlab.dune-project.org/dune-fem/dune-fem.git
[2]: http://arxiv.org/abs/1604.07242
[3]: http://www.dune-project.org
[4]: http://www.dune-project.org/doc/installation-notes.html
[5]: LICENSE.md